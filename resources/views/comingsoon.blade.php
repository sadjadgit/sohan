<!DOCTYPE html>
<html lang="en">
@include('layouts.site.blocks.head')

<body style="direction:ltr!important;">
	<div class="bgimg">
		<div class="topleft">
			<p class="text-white m-0 p-3">
				<img src="{{asset('assets/site/image/logo.png')}}" height="40" alt="" class="">
			</p>
		</div>
		<div class="position-absolute h-100 w-100 d-flex align-items-center justify-content-center">
			<div class="text-center">
				<h1 class="my-2 fw-bolder">
					coming soon
				</h1>
				<hr class="my-3">
				<h3 class="my-2 fw-bolder">
					35 days left
				</h3>
			</div>
		</div>
		<div class="bottomleft">
			<a href="tel:+09123456789" class="text-decoration-none">
				<p class="text-dark fw-bolder bg-white px-4 shadow">
					09123456789
				</p>
			</a>
		</div>
	</div>
	@include('layouts.site.blocks.script')
</body>

</html>