@extends('layouts.admin.master')
@section('title','محصولات')
@section('content')
<div class="col-lg-10 mx-auto">
	<h3 class="bg-white py-2 px-4 rounded-lg">
		محصولات
	</h3>
	<div class="card card-default p-3 rounded-lg">
		<form method="post" action="{{url('/admin/products/delete')}}" style="float: left">
			{{ csrf_field() }}
			<div class="py-3 float-left">
				<button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید؟');"
					data-toggle="tooltip" data-original-title="Delete selected items"
					class="btn btn-danger btn-xs text-white" style="color:#fd5d93">
					حذف انتخاب شده ها
				</button>
				<a href="{{url('admin/products/add')}}" type="button" class="btn btn-success">
					+جدید
				</a>
			</div>
			<div class="table-responsive rounded-lg border">
				<table class="table table-bordered table-hover rounded-lg overflow-hidden" id="table-ext-1">
					<thead>
						<tr>
							<th class="text-center">
								<input id="check-all" style="opacity: 1;position:static;" type="checkbox" />
							</th>
							<th class="text-center">ID</th>
							<th class="text-center">تصویر</th>
							<th class="text-center">نام </th>
							<th class="text-center">دسته</th>
							<th class="text-center">وضعیت</th>
							<th class="text-center">عملیات</th>
						</tr>
					</thead>
					<tbody>
						@foreach($products as $key => $product)
						<tr>
							<td class="text-center">
								<input style="opacity: 1;position:static;" name="deleteId[]" class="delete-all"
									type="checkbox" value="{{$product->id}}" />
							</td>
							<td class="text-center">
								{{$key+1}}
							</td>
							<td class="text-center">
								<div class="media">
									<img class="w-25 m-auto"
										src="{{asset('assets/uploads/'.$product->image)}}"
										style="width:40px" alt="Image">
								</div>
							</td>
							<td class="text-center">
								{{$product->title}}
							</td>
							<td class="text-center">
								{{@$product->category->title}}
							</td>

							<td class="text-center">@if($product->status) نمایش در صفحه اول@else عدم نمایش در
								صفحه
								اول@endif </td>
							<td class="text-center">
								<a href="{{URL::action('Admin\ProductController@getEditProduct',$product->id)}}"
									type="button" class="btn btn-warning btn-circle" data-toggle="tooltip" title="ویرایش">
									<i class="fa fa-edit"> </i>
								</a>
								<a href="{{URL::action('Admin\ProductController@getImage',$product->id)}}"
								   type="button" class="btn btn-success btn-circle" data-toggle="tooltip" title="تصاویر بیشتر">
									<i class="fa fa-picture-o"> </i>
								</a>
								<a href="{{url('/'.@$product->url)}}" target="_blank" type="button"
									class="btn btn-primary btn-circle" data-toggle="tooltip" title="نمایش در سایت">
									<i class="fa fa-eye"></i>
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				<div class="pagii">
					@if(count($products))
					{!! $products->appends(Request::except('page'))->render() !!}
					@endif
				</div>
			</div>
		</form>
	</div>
</div>
@stop

@section('js')
<script>
$(document).ready(function() {
	$('#check-all').change(function() {
		$(".delete-all").prop('checked', $(this).prop('checked'));
	});
});
</script>
@stop
