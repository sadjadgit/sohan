<div class="row">
	{{ csrf_field() }}

	<div class="col-lg-6 p-2">
		<div class="form-group">
			<label> نام محصول</label>
			<input class="form-control" type="text" name="title"
				value="@if(isset($data->title)){{$data->title}}@endif">
		</div>
	</div>
	<div class="col-lg-6 p-2">
		<div class="form-group">
			<label> نام انگلیسی محصول</label>
			<input class="form-control" type="text" name="title_en"
				   value="@if(isset($data->title_en)){{$data->title_en}}@endif">
		</div>
	</div>
	<div class="col-lg-6 p-2">
		<div class="form-group">
			<label> رنگ </label>
			<input class="form-control " type="color" name="color"
				   value="@if(isset($data->color)){{$data->color}}@endif">
		</div>
	</div>
	<div class="col-lg-6 p-2">
		<div class="form-group">
			<label>url  </label>
			<input class="form-control" type="text" name="url" value="@if(isset($data->url)){{$data->url}}@endif">
		</div>
	</div>
	<div class="col-lg-6 p-2">
		<div class="form-group">
			<label>نام دسته محصول </label>
			<select class="form-control" name="parent_id" value="@if(isset($data->parent_id)){{$data->parent_id}}@endif">
				@foreach($category as $row)
				<option value="{{$row->id}}" @if(isset($data->parent_id))
					@if($data->parent_id==$row->id) selected @endif
					@endif
					>{{$row->title}}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="col-lg-6 p-2">
		<div class="form-group">
			<label> تصویر محصول</label>
			<input class="form-control" type="file" name="image">
			@if(isset($data->image))
			<img src="{{asset('assets/uploads/'.$data->image)}}" style="  height: 80px">
			@endif
		</div>
	</div>
	<div class="col-lg-12 p-2">
		<div class="form-group">
			<label>  توضیحات کوتاه</label>
			<textarea class="form-control ckeditor" type="text" name="lead">@if(isset($data->lead)){!! $data->lead !!}@endif</textarea>
		</div>
	</div>
	<div class="col-lg-12 p-2">
		<div class="form-group">
			<label> توضیحات</label>
			<textarea class="form-control ckeditor" type="text" name="description">@if(isset($data->description)){!! $data->description !!}@endif</textarea>
		</div>
	</div>
	<div class="col-lg-6 p-2">
		<div class="form-group">
			<label> توضیحات سئو</label>
			<textarea class="form-control" type="text" rows="5" name="description_seo">@if(isset($data->description_seo)){{$data->description_seo}}@endif</textarea>
		</div>
	</div>
	<div class="col-lg-6 p-2">
		<div class="form-group">
			<label>عنوان سئو</label>
			<input class="form-control" type="text" name="title_seo"
				value="@if(isset($data->title_seo)){{$data->title_seo}}@endif">
		</div>
	</div>

	<div class="col-lg-6 p-2">
		<div class="form-group">
			<div class="checkbox">
				<input type="checkbox" value="1" @if(isset($data->status) && $data->status == 1) checked="checked"
				@endif
				name="status">
				<label>
					نمایش ویژه در صفحه اول
				</label>
			</div>
		</div>
	</div>
	<div class="col-lg-12 p-2">
		<div class="form-group">
			<button type="submit" class="btn btn-primary">ذخیره</button>
		</div>
	</div>
</div>
