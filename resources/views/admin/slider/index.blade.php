@extends('layouts.admin.master')
@section('title','اسلایدر')
@section('part','محتوا')
@section('content')
<div class="col-lg-10 mx-auto">
	<h3 class="bg-white py-2 px-4 rounded-lg">
		اسلایدر
	</h3>
	<div class="card card-default p-3 rounded-lg">
		<form method="post" action="{{url('/admin/slider/delete')}}" style="float: left">
			{{ csrf_field() }}
			<div class="float-left pb-3 pt-2">
				<button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید؟');"
					data-toggle="tooltip" data-original-title="Delete selected items"
					class="btn btn-danger btn-xs text-white" style="color:#fd5d93">حذف انتخاب شده ها
				</button>
				<a href="{{url('admin/slider/add')}}" type="button" class="btn btn-success">
					+جدید
				</a>
			</div>
			<div class="table-responsive">
				<table class="table table-bordered table-hover" id="table-ext-1">
					<thead>
						<tr>
							<th class="text-center">
								<input id="check-all" style="opacity: 1;position:static;" type="checkbox" />
							</th>
							<th class="text-center">ردیف</th>
							<th class="text-center">تصویر</th>
							<th class="text-center">نام </th>

							<th class="text-center">وضعیت</th>
							<th class="text-center">عملیات</th>
						</tr>
					</thead>
					<tbody>
						@foreach($slider as $key => $sli)
						<tr>
							<td class="text-center">
								<input style="opacity: 1;position:static;" name="deleteId[]" class="delete-all"
									type="checkbox" value="{{$sli->id}}" />
							</td>
							<td class="text-center">
								{{$key+1}}
							</td>
							<td class="text-center">
								<div class="media">
									<img class="w-50 mx-auto"
										src="{{asset('assets/uploads/medium/'.$sli->image)}}"
										alt="Image">
								</div>
							</td>
							<td class="text-center">
								{{$sli->title}}
							</td>

							<td class="text-center">
								@if($sli->status) نمایش در صفحه اول@else عدم نمایش در صفحه اول@endif
							</td>


							<td class="text-center">
								<a href="{{URL::action('Admin\SliderController@getEditSlider',$sli->id)}}"
									type="button" class="btn btn-warning btn-circle">
									<i class="fa fa-edit"></i>
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
                <div class="pagii">
                    @if(count($slider))
                        {!! $slider->appends(Request::except('page'))->render() !!}
                    @endif
                </div>
			</div>
		</form>
	</div>
</div>
@stop

@section('js')
<script>
$(document).ready(function() {
	$('#check-all').change(function() {
		$(".delete-all").prop('checked', $(this).prop('checked'));
	});
});
</script>

@stop
