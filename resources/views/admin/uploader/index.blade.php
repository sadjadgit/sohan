@extends('layouts.admin.master')
@section('title','آپلودر')
@section('content')
<div class="col-lg-10 mx-auto">
	<div class="card card-default rounded-lg p-3">
		<div class="table-responsive rounded-lg overflow-hidden border">
			<table class="table table-bordered table-hover m-0" id="table-ext-1">
				<thead>
					<tr>
						<th class="text-center">ID</th>
						<th class="text-center">تصویر</th>
						<th class="text-center">نام </th>
						<th class="text-center">آدرس </th>
						<th class="text-center">عملیات</th>
						<th class="text-center">
							<a href="{{url('admin/uploader/add')}}" type="button" class="btn btn-danger">
								+جدید
							</a>
						</th>
					</tr>
				</thead>
				<tbody>
                @php use Classes\Helper; @endphp
					@foreach($uploader as $upl)
					<tr>
						<td class="text-center">
							{{$upl->id}}
						</td>
						<td class="text-center">
							<div class="media">
								<img class="img-fluid circle"
									src="{{asset('assets/uploads/content/upl/medium/'.$upl->image)}}"
									alt="Image">
							</div>
						</td>
						<td class="text-center">
							{{$upl->title}}
						</td>
						<td class="text-center">
                            {{asset('assets/uploads/content/upl/big/'.Helper::persian2LatinDigit($upl->image))}}
						</td>
						<td class="text-center">
							<a href="{{URL::action('Admin\ContentController@getEditUploader',$upl->id)}}"
								type="button" class="btn btn-warning btn-circle">
								<i class="fa fa-edit"></i>
							</a>
							<a href="{{URL::action('Admin\ContentController@getDeleteUploader',$upl->id)}}"
								type="button" class="btn btn-danger btn-circle">
								<i class="fa fa-trash"></i>
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@stop
