{{ csrf_field() }}
<div class="row">
	<div class="col-lg-6 p-2">
		<div class="form-group">
			<label>نام </label>
			<input class="form-control" type="text" name="title"
				value="@if(isset($data->title)) {{$data->title}} @endif">
		</div>
	</div>
{{--	<div class="col-lg-6 p-2">--}}
{{--		<div class="form-group">--}}
{{--			<label>url</label>--}}
{{--			<input class="form-control" type="text" name="url" value="@if(isset($data->url)) {{$data->url}} @endif">--}}
{{--		</div>--}}
{{--	</div>--}}
	<div class="col-lg-6 p-2">
		<div class="form-group">
			<label>:تصویر </label>
			<input class="form-control" type="file" name="image">
			@if(isset($data->image)) <img src="{{asset('assets/uploads/content/upl/small/'.$data->image)}}"> @endif
		</div>
	</div>
	<div class="col-lg-12 p-2">
		<div class="box-footer">
			<button type="submit" class="btn btn-primary">ذخیره</button>
		</div>
	</div>
</div>