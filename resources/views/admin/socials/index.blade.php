@extends('layouts.admin.master')
@section('title','صفحات اجتماعی')
@section('content')
<div class="col-lg-10 mx-auto">
    <div class="card card-default rounded-lg overflow-hidden border-0" >
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="table-ext-1">
                <thead>
                    <tr>

                        <th class="text-center">اینستاگرام</th>
                        <th class="text-center">واتساپ</th>
                        <th class="text-center">تلگرام</th>
                        <th class="text-center">عملیات</th>
                        @if(!$social)
                        <th class="text-center">
                            <a href="{{url('admin/socials/add')}}" type="button" class="btn btn-green btn-circle">
                                <i class="fa fa-plus"></i>
                            </a>
                        </th>
                            @endif
                    </tr>
                </thead>
                <tbody>
                  
                    <tr>

                        <td class="text-center">{{@$social->instagram}}</td>
                        <td class="text-center">{{@$social->whatsapp}}</td>
                        <td class="text-center">{{@$social->telegram}} </td>

                        <td class="text-center">
                            @if($social)
                            <a href="{{URL::action('Admin\SocialController@getEditSocial',$social->id)}}"
                                type="button" class="btn btn-warning btn-circle">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a href="{{URL::action('Admin\SocialController@getDeleteSocial',$social->id)}}"
                                type="button" class="btn btn-danger btn-circle">
                                <i class="fa fa-trash"></i>
                            </a>
                            @endif
                        </td>

                    </tr>
             
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop
