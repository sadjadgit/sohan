{{ csrf_field() }}
<div class="row">
	<div class="col-md-6 p-2">
		<div class="form-group">
			<div class="col-md-12 p-0">
				<label>تلگرام :</label>
				<input class="form-control bg-light rounded-0" type="text" name="telegram"
					value="@if(isset($data->telegram)) {{$data->telegram}} @endif">
			</div>
		</div>
	</div>
	<div class="col-md-6 p-2">
		<div class="form-group">
			<div class="col-md-12 p-0">
				<label>اینستاگرام :</label>
				<input class="form-control bg-light rounded-0" type="text" name="instagram"
					   value="@if(isset($data->instagram)) {{$data->instagram}} @endif">
			</div>
		</div>
	</div>
	<div class="col-md-6 p-2">
		<div class="form-group">
			<div class="col-md-12 p-0">
				<label>واتساپ :</label>
				<input class="form-control bg-light rounded-0" type="text" name="whatsapp"
					   value="@if(isset($data->whatsapp)) {{$data->whatsapp}} @endif">
			</div>
		</div>
	</div>
	<div class="col-md-12 p-2">
		<div class="form-group">
			<button type="submit" class="btn btn-primary">
				ذخیره
			</button>
		</div>
	</div>
</div>
