{{ csrf_field() }}
<div class="row">
	<div class="col-lg-12 p-2">
		<div class="form-group">
			<label class="col-form-label" style="float: right;">آدرس قديمي</label>
			<input type="text" name="old_address"
				value="@if(isset($data->old_address)) {{$data->old_address}} @endif" class="form-control">
		</div>
	</div>
	<div class="col-lg-12 p-2">
		<div class="form-group">
			<label class="col-form-label" style="float: right;">آدرس جديد</label>
			<input type="text" name="new_address"
				value="@if(isset($data->new_address)) {{$data->new_address}} @endif" class="form-control">
		</div>
	</div>
	<div class="col-lg-12 p-2">
		<div class="form-group">
			<button class="btn btn-success animation-on-hover" type="submit" rel="tooltip"
				data-placement="bottom">ذخیره</button>
		</div>
	</div>
</div>