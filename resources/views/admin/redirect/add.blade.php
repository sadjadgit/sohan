@extends('layouts.admin.master')
@section('title','جدید')
@section('content')
<div class="col-lg-10 mx-auto py-4">
	<h3 class="bg-white py-2 px-4 rounded-lg">
		اضافه کردن سوال جدید
	</h3>
	<div class="card rounded-lg border-0 p-3">
		<form method="post" enctype="multipart/form-data" id="rahweb_form"
			action="{{URL::action('Admin\RedirectController@postRedirectAdd')}}">
			@include('admin.redirect.form')
		</form>
	</div>
</div>
@endsection