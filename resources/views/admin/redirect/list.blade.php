@extends('layouts.admin.master')
@section('title','ریدایرکت')
@section('content')
<div class="col-lg-10 mx-auto">
	<h3 class="bg-white py-2 px-4 rounded-lg">
		لیست آدرس ها
	</h3>
	<div class="card rounded-lg overflow-hidden p-3">
		<div class="card-header">
			<div class="tools float-left">
				<div class="dropdown">
					<button type="button" class="btn btn-default dropdown-toggle btn-link btn-icon"
						data-toggle="dropdown" aria-expanded="false">
						<i class="tim-icons icon-settings-gear-63"></i>
					</button>
					<div class="dropdown-menu dropdown-menu-left" x-placement="bottom-end"
						style="position: absolute; transform: translate3d(-132px, 22px, 0px); top: 0px; left: 0px; will-change: transform;">
						<a class="dropdown-item" href="{{URL::action('Admin\RedirectController@getRedirectAdd')}}"
							style="color:#00c200">
							اضافه کردن ایتم جدید
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="card-body">
			<div class="table-responsive overflow-auto rounded-lg border">
				<table class="table table-bordered table-hover rounded-lg overflow-hidden">
					<thead>
						<tr>
							<th class="text-center">
								ادرس قديم
							</th>
							<th class="text-center">
								آدرس جديد
							</th>
							<th class="text-center">
								تاریخ
							</th>
							<th class="text-center">
								عملیات
							</th>
						</tr>
					</thead>
					<tbody>
						<div class="pagii">
							@if(count($redirect))
							{!! $redirect->appends(Request::except('page'))->render() !!}
							@endif
						</div>
                        @php use Classes\Helper; @endphp
						@foreach($redirect as $row)
						<tr>
							<td class="text-center">
								<p style="float: right;color:#62687a">
                                    {{ Helper::persian2LatinDigit($row->old_address)}}
								</p>
							</td>
							<td class="text-center">
								<p style="float: right;color:#62687a">
                                    {{ Helper::persian2LatinDigit($row->new_address)}}
								</p>
							</td>
							<td class="text-center">
								<p style="float: right;color:#62687a">
									{{$row->created_at}}
								</p>
							</td>
							<td class="text-right">
								<a href="{{URL::action('Admin\RedirectController@getRedirectDelete',$row->id)}}"
									type="button" class="btn btn-danger btn-circle">
									<i class="fa fa-trash"></i>
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				<div class="pagii">
					@if(count($redirect))
					{!! $redirect->appends(Request::except('page'))->render() !!}
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
