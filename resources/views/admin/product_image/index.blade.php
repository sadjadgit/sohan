@extends('layouts.admin.master')
@section('title','تصاویر محصولات')
@section('content')
    <div class="col-lg-10 mx-auto">
        <h3 class="bg-white py-2 px-4 rounded-lg">
            تصاویر محصول
        </h3>
        <div class="card p-3 rounded-lg card-default">
            <form method="post" action="{{URL::action('Admin\ProductController@postDeleteImage')}}" style="float: left">
                {{ csrf_field() }}
                <div class="float-right pb-3 pt-2">
                    <button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید؟');"
                            data-toggle="tooltip" data-original-title="Delete selected items"
                            class="btn btn-danger btn-xs text-white" style="color:#fd5d93">حذف انتخاب شده ها
                    </button>
                    <a href="{{URL::action('Admin\ProductController@getAddImage' , $image->id)}}" type="button" class="btn btn-success m-2">  +جدید
                    </a>
                </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id="table-ext-1">
                            <thead>
                            <tr>
                                <th class="text-center">
                                    <input id="check-all" style="opacity: 1;position:static;" type="checkbox" />
                                </th>
                                <th class="text-center">تصویر</th>
                                <th class="text-center">مربوط به محصول:</th>
{{--                                <th class="text-center">عملیات</th>--}}
                            </thead>
                            <tbody>
                            @foreach($data as $row)
                                <tr>
                                    <td class="text-center">
                                        <input style="opacity: 1;position:static;" name="deleteId[]" class="delete-all"
                                               type="checkbox" value="{{$row->id}}" />
                                    </td>
                                    <td class="text-center">
                                        <div class="media">
                                            <img class=" mx-auto" src="{{asset('assets/uploads/'.$row->image)}}" alt="Image" style="height: 50px;">
{{--                                            <img class="w-50 mx-auto" src="{{asset('assets/uploads/content/pro/'.$row->image)}}" alt="Image" style="height: 50px;">--}}
                                        </div>
                                    </td>
                                    <td class="text-center">{{@$row->content->title}}</td>
{{--                                    <td class="text-center">--}}
{{--                                        <a href="{{URL::action('Admin\ProductController@getEditImage' , $row->id)}}" type="button" class="btn btn-warning btn-circle"><i class="fa fa-edit"> </i></a>--}}
{{--                                    </td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>


            </form>
        </div>
    </div>
@stop
@section('js')

    <script>
        $(document).ready(function () {
            $('#check-all').change(function () {
                $(".delete-all").prop('checked', $(this).prop('checked'));
            });
        });
    </script>

@stop
