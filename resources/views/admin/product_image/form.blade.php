<div class="row">
    {{ csrf_field() }}
    <div class="col-lg-12 p-2">
        <div class="form-group">
            <a href="{{URL::action('Admin\ContentController@getCropper')}}" type="button" class="btn btn-success btn-circle" target="_blank"><i class="fa fa-crop"> </i></a>
        </div>
    </div>
    <div class="col-lg-6 p-2">
        <div class="form-group">
            <input type="hidden" name="content_id" value="{{$product_id}}">
            <div class="col-md-12 p-0">
                <label> تصویر <span class="text-danger">(size:444*751)</span> :</label>
                <input class="form-control" type="file" name="image" required>
                @if(isset($data->image)) <img src="{{asset('assets/uploads/'.$data->image)}}"
                                              style="height: 150px; width: 100%"> @endif
{{--                @if(isset($data->image)) <img src="{{asset('assets/uploads/content/pro/'.$data->image)}}"--}}
{{--                                              style="height: 150px; width: 100%"> @endif--}}

            </div>
        </div>

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">ذخیره</button>
        </div>
    </div>
</div>
