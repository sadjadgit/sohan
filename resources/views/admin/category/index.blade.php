@extends('layouts.admin.master')
@section('title','دسته بندی')
@section('content')
<div class="col-lg-10 mx-auto">
	<h3 class="bg-white py-2 px-4 rounded-lg">
		دسته بندی محصولات
	</h3>
	<div class="card card-default rounded-lg p-3 border-0">
		<form method="post" action="{{url('/admin/category/delete')}}" style="float: left">
			{{ csrf_field() }}
			<div class="float-left pb-3 py-2">
				<button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید؟');"
					data-toggle="tooltip" data-original-title="Delete selected items"
					class="btn btn-danger btn-xs text-white" style="color:#fd5d93">حذف انتخاب شده ها
				</button>
				<a href="{{url('admin/category/add')}}" type="button" class="btn btn-success">
					+جدید
				</a>
			</div>
			<!-- START table-responsive-->
			<div class="table-responsive">
				<table class="table table-bordered table-hover" id="table-ext-1">
					<thead>
						<tr>
							<th class="text-center">
                                <input id="check-all" style="opacity: 1;position:static;" type="checkbox" />
							</th>
							<th class="text-center">ردیف</th>
							<th class="text-center">تصویر</th>

							<th class="text-center">نام </th>

							<th class="text-center">وضعیت</th>
							<th class="text-center">عملیات</th>

						</tr>
					</thead>
					<tbody>
						@foreach($category as $key => $cat)
						<tr>
							<td class="text-center">
								<input style="opacity: 1;position:static;" name="deleteId[]" class="delete-all" type="checkbox" value="{{$cat->id}}" />
							</td>
							<td class="text-center">
								{{$key+1}}
							</td>
							<td class="text-center">
								<div class="media">
									<img class="w-25 mx-auto" src="{{asset('assets/uploads/small/'.$cat->image)}}" alt="Image" style="height:50px;">
								</div>
							</td>

							<td class="text-center">
								{{$cat->title}}
							</td>

							<td class="text-center">
								@if($cat->status) فعال@else غیرفعال@endif
							</td>
							<td class="text-center">
								<a href="{{URL::action('Admin\CategoryController@getEditCategory',$cat->id)}}"
									type="button" class="btn btn-warning btn-circle" data-toggle="tooltip" title="ویرایش">
									<i class="fa fa-edit"></i>
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				<div class="pagii">
					@if(count($category))
					{!! $category->appends(Request::except('page'))->render() !!}
					@endif
				</div>
			</div>
		</form>
	</div>
</div>
@stop

@section('js')
<script>
$(document).ready(function() {
	$('#check-all').change(function() {
		$(".delete-all").prop('checked', $(this).prop('checked'));
	});
});
</script>
@stop
