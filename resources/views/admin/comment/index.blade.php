@extends ("layouts.admin.master")
@section('title','کامنت ها')
@section('part','کامنت ها')
@section('content')
<div class="row">
	@include('layouts.admin.blocks.message')
	<div class="col-lg-10 mx-auto">
		<div class="box card shadow border-0 rounded p-3">
			<div class="pb-3 pt-2 float-left text-left">
				<a href="{{URL::action('Admin\CommentController@getExcel')}}" data-toggle="tooltip"
					class="btn btn-primary btn-lg">
					<i class="fa fa-file-excel-o"></i>
				</a>
			</div>
			<div class="box-body rounded-lg overflow-hidden border table-responsive table-responsive-sm table-responsive-md table-responsive-lg table-responsive-xl no-padding rounded overflow-hidden">
				<table class="table table-hover table-bordered rounded m-0">
					<thead>
						<tr>
							<th class="text-center">متعلق به</th>
							<th class="text-center">ایمیل</th>
							<th class="text-center">تاریخ</th>
							<th class="text-center">نمایش در صفحه</th>
							<th class="text-center">وضعیت</th>
							<th class="text-center">عملیات</th>
						</tr>
					</thead>
					<tbody>
						@foreach($data as $row)
						<tr>
							<td class="text-center">
								{{@$row->article->title}}

							</td>
							<td class="text-center">
								{{$row->email}}

							</td>
							<td class="text-center">
								{{jdate('Y/m/d H:i',$row->created_at->timestamp)}}
							</td>
							<td class="text-center">
								@if($row->status==1)
								<span class='label label-success'>فعال</span>
								@else
								<span class='label label-danger'>غیر فعال</span>
								@endif
							</td>
							<td class="text-center">
								@if($row->readat==1)
								<span class='label label-success'>خوانده شده</span>
								@else
								<span class='label label-danger'>خوانده نشده </span>
								@endif
							</td>
							<td>
								<center>
									<a href="{{URL::action('Admin\CommentController@getEdit',$row->id)}}"
										data-toggle="tooltip" data-original-title="ویرایش اطلاعات"
										class="btn btn-warning btn-xs">
										<i class="fa fa-edit"></i>
									</a>
								</center>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				<center>
					@if(count($data))
					{!! $data->appends(Request::except('page'))->render() !!}
					@endif
				</center>
			</div>
		</div>
	</div>
</div>

@stop
