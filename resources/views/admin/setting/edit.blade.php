@extends('layouts.admin.master')
@section('title','تنظیمات')
@section('content')
<div class="col-md-10 text-right mx-auto pb-5 pt-3">
	<h3 class="bg-white py-2 px-4 rounded-lg">
		تنظیمات
	</h3>
	<form method="post" action="{{URL::action('Admin\SettingController@postEditSetting',$data->id)}}"
		enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="card rounded-lg">
			<div class="card-header d-flex p-3 ui-sortable-handle" style="cursor: move;">
				<ul class="nav nav-pills ml-auto">
					<li class="nav-item">
						<a class="nav-link btn" href="#revenue-chart" data-toggle="tab">گرافیکی</a>
					</li>
					<li class="nav-item mr-3">
						<a class="nav-link btn active show" href="#sales-chart" data-toggle="tab">عمومی</a>
					</li>
				</ul>
				<h3 class="mr-auto">
					<i class="fa fa-gears"></i>
					تنظیمات
				</h3>
			</div>
			<div class="card-body">
				<div class="tab-content p-0">
					<div class="chart tab-pane" id="revenue-chart" style="position: relative; height: 100%;">
						<div class="row">
							<div class="col-lg-12 p-2">
								<div class="form-group">
									<label>تصویر درباره ما :</label>
									<input class="form-control" type="file" name="aboutimg">
									@if(isset($data->aboutimg))
									<img src="{{asset('assets/uploads/content/set/'.$data->aboutimg)}}"
										style="width: 200px;">
									@endif
								</div>
							</div>
							<div class="col-lg-12 p-2">
								<div class="form-group">
									<label>لوگو :</label>
									<input class="form-control" type="file" name="logo">
									@if(isset($data->logo))
									<img src="{{asset('assets/uploads/'.$data->logo)}}"
										style="width: 200px;">
									@endif
								</div>
							</div>
							<div class="col-lg-12 p-2">
								<div class="form-group">
									<label>لوگوی فوتر :</label>
									<input class="form-control" type="file" name="logo_footer">
									@if(isset($data->logo_footer))
										<img src="{{asset('assets/uploads/'.$data->logo_footer)}}"
											 style="width: 200px;">
									@endif
								</div>
							</div>
							<div class="col-lg-12 p-2">
								<div class="form-group">
									<label>آیکن :</label>
									<input class="form-control" type="file" name="favicon">
									@if(isset($data->favicon))
									<img src="{{asset('assets/uploads/content/set/'.$data->favicon)}}"
										style="width: 200px;">
									@endif
								</div>
							</div>
							<div class="col-lg-12 p-2">
								<div class="form-group">
									<label>Google Map :</label>
									<textarea rows="3" class="form-control" name="maps" style="direction: ltr">@if(isset($data->maps)){!!$data->maps!!}@endif</textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="chart tab-pane active show" id="sales-chart">
						<div class="row">
							<div class="col-lg-6 p-2">
								<div class="form-group">
									<label>عنوان سایت :</label>
									<input class="form-control" type="text" name="title" value="@if(isset($data->title)){{$data->title}}@endif"/>
								</div>
							</div>
							<div class="col-lg-6 p-2">
								<div class="form-group">
									<label>عنوان درباره ما :</label>
									<input class="form-control" type="text" name="abouttitle" value="@if(isset($data->abouttitle)){{$data->abouttitle}}@endif"/>
								</div>
							</div>
							<div class="col-lg-12 p-2">
								<div class="form-group">
									<label>درباره ما  :</label>
									<textarea rows="3" class="form-control ckeditor" name="about">@if(isset($data->about)) {!!$data->about!!} @endif</textarea>
								</div>
							</div>
							<div class="col-lg-12 p-2">
								<div class="form-group">
									<label>درباره ما صفحه اول </label>
									<textarea rows="3" class="form-control" name="footer_about">@if(isset($data->footer_about)){!!$data->footer_about!!}@endif</textarea>
								</div>
							</div>
							<div class="col-lg-6 p-2">
								<div class="form-group">
									<label>تلفن :</label>
									<input class="form-control" type="text" name="contact" value="@if(isset($data->contact)){{$data->contact}}@endif"/>
								</div>
							</div>
							<div class="col-lg-6 p-2">
								<div class="form-group">
									<label>تلفن2 :</label>
									<input class="form-control" type="text" name="phone" value="@if(isset($data->phone)){{$data->phone}}@endif"/>
								</div>
							</div>
							<div class="col-lg-6 p-2">
								<div class="form-group">
									<label>کد پستی :</label>
									<input class="form-control" type="text" name="postal_code" value="@if(isset($data->postal_code)){{$data->postal_code}}@endif"/>
								</div>
							</div>
							<div class="col-lg-6 p-2">
								<div class="form-group">
									<label>آدرس واحد فروش:</label>
									<textarea rows="3" class="form-control" type="text" name="address">@if(isset($data->address)){!!$data->address!!}@endif</textarea>
								</div>
							</div>

							<div class="col-lg-6 p-2">
								<div class="form-group">
									<label>آدرس کارخانه:</label>
									<textarea rows="3" class="form-control" type="text" name="factory_address">@if(isset($data->factory_address)){!!$data->factory_address!!}@endif</textarea>
								</div>
							</div>

							<div class="col-lg-6 p-2">
								<div class="form-group">
									<label>E-mail :</label>
									<input class="form-control" type="text" name="email" value="@if(isset($data->email)){{$data->email}}@endif">
								</div>
							</div>
							<div class="col-lg-6 p-2">
								<div class="form-group">
									<label>توضیحات سئو :</label>
									<textarea rows="3" class="form-control" type="text" name="description_seo">@if(isset($data->description_seo)){!!$data->description_seo!!}@endif</textarea>
								</div>
							</div>

						</div>

					</div>
                    <div class="col-lg-12 p-2">
                        <div class="form-group">
                            <input class="btn btn-lg btn-success" type="submit" value="ذخیره">
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</form>
</div>
@stop
