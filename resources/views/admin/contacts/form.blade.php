<div class="card rounded-lg border-0 p-3">
    {{ csrf_field() }}
    <div class="box-body">

        <div class="form-group">
            <div class="row">
                <div class="col-md-4">
                <label>نام:</label>
                <input class="form-control" type="text" id="name" name="name"
                       value="@if(isset($data->name)) {{$data->name}} @endif"
                       placeholder="عنوان  را وارد کنید . . ." >
                </div>




                <div class="col-md-4">
                <label>ایمیل:</label>
                <input class="form-control" type="text" id="email" name="email"
                       value="@if(isset($data->email)) {{$data->email}} @endif"
                       placeholder="عنوان  را وارد کنید . . ." disabled>
                </div>
            </div>
        </div>
    <div class="form-group">
        <label>موضوع:</label>
        <textarea class="form-control" id="subject" name="subject"
        >@if(isset($data->subject)) {{$data->subject}} @endif</textarea>
    </div>


    <div class="form-group">
        <label>توضیحات:</label>
		<textarea class="form-control" id="message" name="message"
                  >@if(isset($data->message)) {{$data->message}} @endif</textarea>
    </div>

    </div>

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">ذخیره</button>
    </div>
</div>
