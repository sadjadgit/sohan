@extends ("layouts.admin.master")
@section('title','تماس با ما')
@section('content')
    <div class="row">
        @include('layouts.admin.blocks.message')
        <div class="col-lg-10 mx-auto">
            <div class="box card shadow border-0 rounded p-3">
{{--                <div class="pb-3 pt-2 float-left text-left">--}}
{{--                    <a href="{{URL::action('Admin\ContactController@getExcel')}}" data-toggle="tooltip"--}}
{{--                       class="btn btn-primary btn-lg">--}}
{{--                        <i class="fa fa-file-excel-o"></i>--}}
{{--                    </a>--}}
{{--                </div>--}}
                <div class="box-body rounded-lg overflow-hidden border table-responsive table-responsive-sm table-responsive-md table-responsive-lg table-responsive-xl no-padding rounded overflow-hidden">
                    <table class="table table-hover table-bordered rounded m-0">
                        <thead>
                        <tr>
{{--                            <th class="text-center">--}}
{{--                              --}}
{{--                                    <input id="check-all" style="opacity: 1;position:static;" type="checkbox"/>--}}
{{--                              --}}
{{--                            </th class="text-center">--}}
                            <th class="text-center">نام</th>
                            <th class="text-center">موضوع</th>
                            <th class="text-center">ایمیل</th>
                            <th class="text-center">تاریخ</th>

                            <th class="text-center">وضعیت</th>

                            <th class="text-center">عملیات</th>
                        </tr>
                        @foreach($data as $row)
                            <tr>
{{--                                <td class="text-center">--}}
{{--                                    <center>--}}
{{--                                        <input style="opacity: 1;position:static;" name="deleteId[]" class="delete-all"--}}
{{--                                               type="checkbox"--}}
{{--                                               value="{{$row->id}}"/>--}}

{{--                                    </center>--}}
{{--                                </td>--}}
                                <td class="text-center">{{@$row->name}}</td>
                                <td class="text-center">{{@$row->subject}}</td>
                                <td class="text-center">{{$row->email}}</td>


                                <td class="text-center">{{jdate('Y/m/d H:i',$row->created_at->timestamp)}}</td>


                                <td class="text-center">

                                        @if($row->readat==1)
                                            <span class='label label-success'>خوانده شده</span>
                                        @else
                                            <span class='label label-danger'>خوانده نشده </span>
                                        @endif

                                </td>

                                <td class="text-center">

                                        <a href="{{URL::action('Admin\ContactController@getEdit',$row->id)}}" data-toggle="tooltip"
                                           data-original-title="ویرایش اطلاعات" class="btn btn-warning  btn-xs"><i
                                                    class="fa fa-edit"></i></a>

                                </td>
                            </tr>

                        @endforeach

                    </table>
                    </form>
                    <center>
                        @if(count($data))
                            {!! $data->appends(Request::except('page'))->render() !!}
                        @endif
                    </center>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>

{{--        <div class="col-xs-4">--}}
{{--            <div id="list">--}}
{{--                <div class="alert alert-info alert-dismissable" style="direction: rtl; margin: 0px auto;">--}}
{{--                    <i class="fa fa-check"></i>--}}
{{--                    <span style="font-size: 14px;">	با درگ کردن ترتیب مورد نظر را انتخاب نمایید.  </span>--}}
{{--                </div>--}}

{{--                <div id="response"></div>--}}
{{--                <ul>--}}
{{--                    @foreach($all as $rowSort)--}}

{{--                        <li id="arrayorder_{!! stripslashes($rowSort['id']) !!}">{!! stripslashes($rowSort['title']) !!}--}}
{{--                            <div class="clear"></div>--}}
{{--                        </li>--}}

{{--                    @endforeach--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--        </div>--}}

    </div>



{{--    <div class="modal fade" id="search" tabindex="-1" role="dialog" aria-labelledby="messageModalLabel" aria-hidden="true">--}}
{{--        <form method="GET" action="{{URL::current()}}"class="form-horizontal">--}}
{{--            {{ csrf_field() }}--}}
{{--            <input type="hidden" name="search" value="search">--}}
{{--            <div class="modal-dialog" style="direction: rtl;">--}}
{{--                <div class="modal-content">--}}
{{--                    <div class="modal-header">--}}
{{--                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                            <span aria-hidden="true">&times;</span>--}}
{{--                        </button>--}}
{{--                        <h4 class="modal-title" id="messageModalLabel"--}}
{{--                            style="direction: rtl; text-align: right; padding-right: 20px;"><i class="fa fa-search"></i> جستجو--}}
{{--                        </h4>--}}
{{--                    </div>--}}
{{--                    <div class="modal-body" style="text-align: justify;">--}}
{{--                        <div class="widget flat radius-bordered">--}}
{{--                            <div class="widget-body">--}}
{{--                                <div id="registration-form">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="col-lg-3 control-label">عنوان:</label>--}}
{{--                                        <div class="col-lg-9">--}}
{{--                                            <input class="form-control" type="text" id="name" name="name">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <button type="submit" data-toggle="tooltip" data-original-title="جستجو" class="btn btn-blue">جستجو--}}
{{--                        </button>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </form>--}}
{{--    </div>--}}


@stop
{{--@section('css')--}}
{{--    <style>--}}
{{--        ul {--}}
{{--            padding: 0px;--}}
{{--            margin: 0px;--}}
{{--        }--}}

{{--        #response {--}}
{{--            padding: 10px;--}}
{{--            background-color: #9F9;--}}
{{--            border: 2px solid #396;--}}
{{--            margin-bottom: 20px;--}}
{{--        }--}}

{{--        #list li {--}}
{{--            margin: 0 0 3px;--}}
{{--            padding: 8px;--}}
{{--            background-color: #333;--}}
{{--            color: #fff;--}}
{{--            list-style: none;--}}
{{--        }--}}
{{--    </style>--}}

{{--    <link href="{{ asset('assets/admin/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">--}}
{{--@stop--}}


{{--@section('js')--}}

{{--    <script src="{{ asset('assets/admin/js/bootstrap-datepicker.min.js')}}"></script>--}}
{{--    <script src="{{ asset('assets/admin/js/bootstrap-datepicker.fa.min.js')}}"></script>--}}

{{--    <script>--}}

{{--        $(".date").datepicker({--}}
{{--            changeMonth: true,--}}
{{--            changeYear: true,--}}
{{--            isRTL: true--}}
{{--        });--}}

{{--        $(document).ready(function () {--}}
{{--            $('#check-all').change(function () {--}}
{{--                $(".delete-all").prop('checked', $(this).prop('checked'));--}}
{{--            });--}}
{{--        });--}}
{{--    </script>--}}

{{--    <meta name="csrf-token" content="{!! csrf_token() !!}"/>--}}
{{--    <script type="text/javascript">--}}
{{--        $(document).ready(function () {--}}
{{--            function slideout() {--}}
{{--                setTimeout(function () {--}}
{{--                    $("#response").slideUp("slow", function () {--}}
{{--                    });--}}

{{--                }, 2000);--}}
{{--            }--}}

{{--            $("#response").hide();--}}
{{--            $(function () {--}}
{{--                $("#list ul").sortable({--}}
{{--                    opacity: 0.8, cursor: 'move', update: function () {--}}
{{--                        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');--}}
{{--                        var order = $(this).sortable("serialize") + '&update=update' + '&_token=' + CSRF_TOKEN;--}}
{{--                        $.post("{!!URL::action('Admin\CommentController@postSort')!!} ", order, function (theResponse) {--}}
{{--                            $("#response").html(theResponse);--}}
{{--                            $("#response").slideDown('slow');--}}
{{--                            slideout();--}}
{{--                        });--}}

{{--                    }--}}
{{--                });--}}
{{--            });--}}

{{--        });--}}
{{--    </script>--}}


{{--@stop--}}
