@extends('layouts.admin.master')
@section('title','         چرا ما')
@section('content')
    <div class="col-lg-10 mx-auto">
        <h3 class="bg-white py-2 px-4 rounded-lg">
        چرا ما
        </h3>
        <div class="card p-3 rounded-lg card-default">
            <form method="post" action="{{URL::action('Admin\WhyController@postDelete')}}" style="float: left">
                {{ csrf_field() }}
                <div class="float-left pb-3 pt-2">
                    <button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید؟');"
                            data-toggle="tooltip" data-original-title="Delete selected items"
                            class="btn btn-danger btn-xs text-white" style="color:#fd5d93">حذف انتخاب شده ها
                    </button>
                    <a href="{{URL::action('Admin\WhyController@getAdd')}}" type="button" class="btn btn-success"> +جدید
                    </a>
                </div>
        <!-- START table-responsive-->
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="table-ext-1">
                <thead>
                <tr>
                    <th class="text-center">
                        <input id="check-all" style="opacity: 1;position:static;" type="checkbox"/>

                    </th>
                    <th class="text-center">ID</th>
                    <th class="text-center">نام </th>
                    <th class="text-center">عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($insures as $glory)
                <tr>
                    <td>
                        <center>
                            <input style="opacity: 1;position:static;" name="deleteId[]"
                                   class="delete-all"
                                   type="checkbox"
                                   value="{{$glory->id}}"/>

                        </center>
                    </td>
                    <td class="text-center">{{$glory->id}}</td>

                    <td class="text-center">{{$glory->title}}</td>

                    <td class="text-center">
                        <a href="{{URL::action('Admin\WhyController@postEdit',$glory->id)}}" type="button" class="btn btn-warning btn-circle"><i class="fa fa-edit"> </i></a>
{{--                        <a href="{{URL::action('Admin\WhyController@getDelete',$glory->id)}}" type="button" class="btn btn-danger btn-circle"><i class="fa fa-trash"> </i></a>--}}
                    </td>
                </tr>
                <tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagii">
                @if(count($insures))
                    {!! $insures->appends(Request::except('page'))->render() !!}
                @endif
            </div>

        </div>
            </form>
        </div>
    </div>


@stop
@section('js')

    <script>
        $(document).ready(function () {
            $('#check-all').change(function () {
                $(".delete-all").prop('checked', $(this).prop('checked'));
            });
        });
    </script>

@stop
