@extends('layouts.admin.master')
@section('title','جدید')
@section('content')
    <div class="col-lg-10 mx-auto py-4" id="app34534567">
        <h3 class="bg-white py-2 px-4 rounded-lg">
            اضافه کردن
        </h3>
        <div class="card rounded-lg border-0 p-3">
    <form method="post" action="{{URL::action('Admin\WhyController@postAdd')}}" enctype="multipart/form-data">
        <div class="row">
            {{ csrf_field() }}
            <div class="col-lg-6 offset-3 p-2"  v-for="me in number" >
                <div class="form-group">
                    <label>:نام </label>
                    <input class="form-control" type="text"   name="title[]" />
                </div>

            </div>

            <div class="col-lg-6 p-5">
                <a @click="plusMe()" class="btn btn-default btn-success" style="font-size: 9px;">
                    <span class="fa fa-plus"></span>
                </a>
            </div>
            <div class="col-lg-12 p-2">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">ذخیره</button>
                </div>
            </div>
        </div>
    </form>
        </div>
    </div>
@stop
@section('js')
    <script>
        var app = new Vue({
            el: '#app34534567',
            data:{
                number :1
            },
            methods: {
                plusMe: function(){
                    this.number = this.number+1;
                }
            }
        })
    </script>
@endsection
