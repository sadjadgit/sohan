@extends('layouts.admin.master')
@section('title','اخبار')
@section('content')
<div class="col-lg-10 mx-auto">
	<h3 class="bg-white py-2 px-4 rounded-lg">
		مقالات
	</h3>
	<div class="card p-3 rounded-lg card-default">
		<form method="post" action="{{url('/admin/articles/delete')}}" style="float: left">
			{{ csrf_field() }}
			<div class="float-left pb-3 pt-2">
				<button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید؟');"
					data-toggle="tooltip" data-original-title="Delete selected items"
					class="btn btn-danger btn-xs text-white" style="color:#fd5d93">حذف انتخاب شده ها
				</button>
				<a href="{{url('admin/articles/add')}}" type="button" class="btn btn-success"> +جدید
				</a>
			</div>
			<div class="table-responsive">
				<table class="table table-bordered table-hover" id="table-ext-1">
					<thead>
						<tr>
							<th class="text-center">
								<input id="check-all" style="opacity: 1;position:static;" type="checkbox" />
							</th>
							<th class="text-center">ردیف</th>
							<th class="text-center">تصویر</th>
							<th class="text-center">نام </th>

							<th class="text-center">وضعیت</th>
							<th class="text-center">عملیات</th>
						</tr>
					</thead>
					<tbody>
						@foreach($articles as $key => $article)
						<tr>
							<td class="text-center">
								<input style="opacity: 1;position:static;" name="deleteId[]" class="delete-all"
									type="checkbox" value="{{$article->id}}" />
							</td>
							<td class="text-center">{{$key+1}}</td>
							<td class="text-center">
								<div class="media">
									<img class="w-25 mx-auto"
										src="{{asset('assets/uploads/small/'.$article->image)}}"
										style="width:100px" alt="Image">
								</div>
							</td>
							<td class="text-center">{{$article->title}}</td>

							<td class="text-center">
								@if($article->status) نمایش در صفحه @else عدم نمایش در صفحه @endif
							</td>
                            <td class="text-center">
                                <a href="{{URL::action('Admin\ArticleController@getEditArticle',$article->id)}}"
                                   type="button" class="btn btn-warning btn-circle mb-2" data-toggle="tooltip" title="ویرایش">
                                    <i class="fa fa-edit"> </i>
                                </a>
{{--                                <a href="{{URL::action('Admin\ArticleController@getDeleteArticle',$article->id)}}"--}}
{{--                                   type="button" class="btn btn-danger btn-circle">--}}
{{--                                    <i class="fa fa-trash"></i>--}}
{{--                                </a>--}}
                                <a href="{{url('/'.@$article->url)}}" target="_blank" type="button"
                                   class="btn btn-primary btn-circle" data-toggle="tooltip" title="نمایش در سایت">
                                    <i class="fa fa-eye"></i>
                                </a>
                            </td>
						</tr>
						@endforeach
					</tbody>
				</table>
				<div class="pagii">
					@if(count($articles))
					{!! $articles->appends(Request::except('page'))->render() !!}
					@endif
				</div>
			</div>
		</form>
	</div>
</div>

@stop
