<div class="row">
	{{ csrf_field() }}

	<div class="col-lg-6 p-2">
		<div class="form-group">
			<label>نام مقاله</label>
			<input class="form-control" type="text" name="title"
				value="@if(isset($data->title)){{$data->title}}@endif">
		</div>
	</div>
	<div class="col-lg-6 p-2">
		<div class="form-group">
			<label>url  </label>
			<input class="form-control" type="text" name="url" value="@if(isset($data->url)){{$data->url}}@endif">
		</div>
	</div>
{{--	<div class="col-lg-6 p-2">--}}
{{--		<div class="form-group">--}}
{{--			<label> تصویر مقاله</label>--}}
{{--			<input class="form-control" type="file" name="image">--}}
{{--			@if(isset($data->image))--}}
{{--			<img src="{{asset('assets/uploads/content/art/medium/'.$data->image)}}" style="height:  300px">--}}
{{--			@endif--}}
{{--		</div>--}}
{{--	</div>--}}
	<div class="col-lg-6 p-2">
		<div class="form-group">
			<label>تصویر </label>

			<input id="input-preview" name="image_get" type="hidden" />
			<input type="file" name="image" id="image" class="form-control" onchange="readURL(this);"/>
			<div class="image_container">
				<img id="blah" src="@if(isset($data->image)) {{asset('assets/uploads/medium/'.$data->image)}} @endif" alt="your image" style="width:50%" />
			</div>
			<div id="cropped_result"></div>
			<button id="crop_button" type="button">Crop</button>
		</div>
	</div>
	<div class="col-lg-12 p-2">
		<div class="form-group">
			<label> توضیحات</label>
			<textarea class="form-control ckeditor" type="text" name="description">@if(isset($data->description)){{$data->description}}@endif</textarea>
		</div>
	</div>
	<div class="col-lg-6 p-2">
		<div class="form-group">
			<label>توضیحات سئو </label>
			<textarea class="form-control" type="text" rows="5" name="description_seo">@if(isset($data->description_seo)){{$data->description_seo}}@endif</textarea>
		</div>
	</div>
	<div class="col-lg-6 p-2">
		<div class="form-group">
			<label>عنوان سئو</label>
			<input class="form-control" type="text" name="title_seo"
				value="@if(isset($data->title_seo)){{$data->title_seo}}@endif">
		</div>
	</div>

	<div class="col-lg-6 form-group">
		<div class="form-group">
			<div class="sd-ch bg-light p-3" style="border-radius:2vh;height:30vh;overflow:scroll;">
				<label>مرتبط ها:</label>
				@foreach($art as $row)
					<label class="container-sd shadow-sm px-3" style="border-radius:2vh;">
						<input  type="checkbox" name="relates[]" value="{{$row->id}}" @if(isset($data) and
	                          in_array($row->id,$rel)) checked="checked" @endif>
						<span class="checkmark">{{$row->title}}</span>
					</label>
				@endforeach
			</div>
		</div>
	</div>

	<div class="col-lg-12 p-2">
		<div class="form-group">
			<input type="checkbox" value="1" @if(isset($data->status) && $data->status == 1) checked="checked"
			@endif
			name="status">
			<label>
				نمایش ویژه در صفحه اول
			</label>
		</div>
	</div>
	<div class="col-lg-6 p-2">
		<div class="form-group">
			<button type="submit" class="btn btn-primary">ذخیره</button>
		</div>
	</div>
</div>
<script>
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#blah').attr('src', e.target.result)
			};
			reader.readAsDataURL(input.files[0]);
			setTimeout(initCropper, 1000);
		}
	}
	function initCropper(){
		console.log("Came here")
		var image = document.getElementById('blah');
		var cropper = new Cropper(image, {
			aspectRatio: 1 / 1,
			crop: function(e) {
				console.log(e.detail.x);
				console.log(e.detail.y);
			}
		});

		// On crop button clicked
		document.getElementById('crop_button').addEventListener('click', function(){
			var imgurl =  cropper.getCroppedCanvas().toDataURL();
			var img = document.createElement("img");
			img.src = imgurl;

			document.getElementById("input-preview").value =  img.src;

			document.getElementById("cropped_result").appendChild(img);
		})
	}
</script>