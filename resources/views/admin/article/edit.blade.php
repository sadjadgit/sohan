@extends('layouts.admin.master')
@section('title','ویرایش')
@section('content')
<div class="col-lg-10 mx-auto py-4">
	<h3 class="bg-white py-2 px-4 rounded-lg">
		ویرایش
	</h3>
	<div class="card rounded-lg border-0 p-3">
		<form method="post" action="{{URL::action('Admin\ArticleController@postEditArticle',$data->id)}}"
			enctype="multipart/form-data">
			@include('admin.article.form')
		</form>
	</div>
</div>
@stop