<div class="row">
	{{ csrf_field() }}

	<div class="col-lg-6 p-2">
		<div class="form-group">
			<label> عنوان</label>
			<input class="form-control" type="text" name="title"
				value="@if(isset($data->title)){{$data->title}}@endif">
		</div>
	</div>
	<div class="col-lg-6 p-2">
		<div class="form-group">
			<label> تصویر </label>
			<input class="form-control" type="file" name="image">
			@if(isset($data->image))
			<img src="{{asset('assets/uploads/content/sample/'.$data->image)}}" style="height  300px">
			@endif
		</div>
	</div>


	<div class="col-lg-12 p-2">
		<div class="form-group">
			<label> توضیحات </label>
			<textarea class="form-control" type="text" rows="5" name="description"> @if(isset($data->description)){{$data->description}}@endif</textarea>
		</div>
	</div>

	<div class="col-lg-12 p-2">
		<div class="form-group">
			<button type="submit" class="btn btn-primary">ذخیره</button>
		</div>
	</div>
</div>
