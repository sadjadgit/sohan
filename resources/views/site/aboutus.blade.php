@extends('layouts.site.master')
@section('content')

<div class="inn-header position-relative">
	<img src="{{asset('assets/site/images/innheader.jpg')}}" class="w-100">
	<div class="overlay position-absolute bottom-0 end-0 start-0">
		<div class="container">
			<h1 class="fw-bolder text-white">
				درباره ما
			</h1>
			<nav class="p-1" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="{{url('/')}}" class="text-white">
							خانه
						</a>
					</li>
					<li class="breadcrumb-item text-white active" aria-current="page">
						درباره ما
					</li>
				</ol>
			</nav>
		</div>
	</div>
</div>
<div class="about py-5">
	<div class="container">
		<div class="row w-100 m-0">
			<div class="col-sm-12 p-1">
				<div class="desc">
					<p>
						{!! $setting->about !!}
					</p>
				</div>
			</div>
		</div>
	</div>
</div>

@stop