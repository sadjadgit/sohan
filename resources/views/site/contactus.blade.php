@extends('layouts.site.master')
@section('content')

<div class="inn-header position-relative">
	<img src="{{asset('assets/site/images/innheader.jpg')}}" class="w-100">
	<div class="overlay position-absolute bottom-0 end-0 start-0">
		<div class="container">
			<h1 class="fw-bolder text-white">
				تماس باما
			</h1>
			<nav class="p-1" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="{{url('/')}}" class="text-white">
							خانه
						</a>
					</li>
					<li class="breadcrumb-item text-white active" aria-current="page">
						تماس با ما
					</li>
				</ol>
			</nav>
		</div>
	</div>
</div>
<div class="article details py-5">
	<div class="container">
		<div class="row w-100 m-0">
			<div class="col-md-6 p-1">
				<h4 class="fw-bolder">
					فرم ارتباط با ما
				</h4>
				<form action="{{URL::action('Site\HomeController@postContact')}}" method="POST" class="m-0 bg-light p-3">
					@csrf
					<div class="row w-100 m-0">
						<div class="col-sm-12 p-1">
							<label for="" class="form-label">
								نام و نام خانوادگی
							</label>
							<input name="name" type="text" class="form-control" id="" placeholder="">
						</div>
						<div class="col-sm-12 p-1">
							<label for="" class="form-label">
								شماره همراه
							</label>
							<input name="phone" type="tel" class="form-control" id="" placeholder="">
						</div>
						<div class="col-sm-12 p-1">
							<label for="" class="form-label">
								نوشتن پیام
							</label>
							<textarea name="message" class="form-control" id="" rows="4"></textarea>
						</div>
						<div class="col-xl-6 ms-auto p-1">
							<button type="submit" class="btn btn-success w-100">
								ارسال پیام
							</button>
						</div>
					</div>
				</form>
			</div>
			<div class="col-md-6 p-1">
				<h4 class="fw-bolder">
					راه های ارتباط با ما
				</h4>
				<div class="bg-light p-3">
					<div class="row w-100 m-0">
						<div class="col-sm-12 px-1 pt-1 pb-2">
							<p class="m-0 d-flex align-items-center text-dark">
								<i class="bi bi-shop d-flex me-2 my-0 h4"></i>
								واحد فروش : {{$setting->address}}
							</p>
						</div>
						<div class="col-sm-12 px-1 pt-1 pb-2">
							<p class="m-0 d-flex align-items-center text-dark">
								<i class="bi bi-building d-flex me-2 my-0 h4"></i>
								آدرس کارخانه : {{$setting->factory_address}}
							</p>
						</div>
						<div class="col-sm-12 px-1 pt-1 pb-2">
							<p class="m-0 d-flex align-items-center text-dark">
								<i class="bi bi-pin-map d-flex me-2 my-0 h4"></i>
								کد پستی : ۲۳۴۳۵۶۷۸۶۷
							</p>
						</div>
						<div class="col-sm-12 px-1 pt-1 pb-2">
							<a href="mailto:{{$setting->email}}" class="m-0 d-flex align-items-center text-dark">
								<i class="bi bi-envelope d-flex me-2 my-0 h4"></i>
								ایمیل : {{$setting->email}}
							</a>
						</div>
						<div class="col-sm-12 px-1 pt-1 pb-2">
							<a href="tel:{{$setting->contact}}" class="m-0 d-flex align-items-center text-dark">
								<i class="bi bi-telephone d-flex me-2 my-0 h4"></i>
								شماره تماس : {{$setting->contact}}
							</a>
						</div>
						<div class="col-sm-12 px-1 pt-1 pb-2">
							<a href="tel:{{$setting->phone}}" class="m-0 d-flex align-items-center text-dark">
								<i class="bi bi-telephone d-flex me-2 my-0 h4"></i>
								شماره تماس : {{$setting->phone}}
							</a>
						</div>
						<div class="col-sm-12 px-1 py-2">
							<ul class="p-0 m-0">
								<li class="float-start list-unstyled me-2">
									<a href="" class="">
										<i class="bi h3 my-0 bi-whatsapp"></i>
									</a>
								</li>
								<li class="float-start list-unstyled me-2">
									<a href="" class="">
										<i class="bi h3 my-0 bi-telegram"></i>
									</a>
								</li>
								<li class="float-start list-unstyled me-2">
									<a href="" class="">
										<i class="bi h3 my-0 bi-instagram"></i>
									</a>
								</li>
								<li class="float-start list-unstyled me-2">
									<a href="" class="">
										<i class="bi h3 my-0 bi-twitter"></i>
									</a>
								</li>
								<li class="float-start list-unstyled me-2">
									<a href="" class="">
										<i class="bi h3 my-0 bi-facebook"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 p-1">
				{!! $setting->maps !!}
			</div>
		</div>
	</div>
</div>

@stop