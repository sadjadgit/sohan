@extends('layouts.site.master')
@section('content')

    <div class="article list py-5">
        <div class="container">
            <div class="row w-100 m-0">
                  @foreach($news as $row)
                    <div class="col-xl-3 col-lg-4 col-sm-6 col-xs-12 p-1">
                        <a href="{{url('/'.$row->url)}}" class="">
                            <div class="card p-2 shadow-sm bg-light">
                                <!-- cruper 1 * 1 -->
                                <img src="{{asset('assets/uploads/medium/'.$row->image)}}" alt="{{$row->title}}" class="w-100">
                                <h4 class="mb-0 mt-2 text-center text-dark">
                                    {{$row->title}}
                                </h4>
                            </div>
                        </a>
                    </div>
                    @endforeach
                      @foreach($product as $row)
                          <div class="col-xl-3 col-lg-4 col-sm-6 col-xs-12 p-1">
                              <a href="{{url('/'.$row->url)}}" class="">
                                  <div class="card p-2 shadow-sm bg-light">
                                      <!-- cruper 1 * 1 -->
                                      <img src="{{asset('assets/uploads/'.$row->image)}}" alt="{{$row->title}}" class="w-100">
                                      <h4 class="mb-0 mt-2 text-center text-dark">
                                          {{$row->title}}
                                      </h4>
                                  </div>
                              </a>
                          </div>
                      @endforeach
                      @foreach($category as $row)
                          <div class="col-xl-3 col-lg-4 col-sm-6 col-xs-12 p-1">
                              <a href="{{url('/'.$row->url)}}" class="">
                                  <div class="card p-2 shadow-sm bg-light">
                                      <!-- cruper 1 * 1 -->
                                      <img src="{{asset('assets/uploads/medium/'.$row->image)}}" alt="{{$row->title}}" class="w-100">
                                      <h4 class="mb-0 mt-2 text-center text-dark">
                                          {{$row->title}}
                                      </h4>
                                  </div>
                              </a>
                          </div>
                      @endforeach
            </div>
        </div>
    </div>
@stop