@extends('layouts.site.master')
@section('content')

<div class="inn-header position-relative">
	<img src="{{asset('assets/site/images/innheader.jpg')}}" class="w-100">
	<div class="overlay position-absolute bottom-0 end-0 start-0">
		<div class="container">
			<h1 class="fw-bolder text-white">
				{{$article->title}}
			</h1>
			<nav class="p-1" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="{{url('/')}}" class="text-white">
							خانه
						</a>
					</li>
					<li class="breadcrumb-item">
						<a href="{{url('/news')}}" class="text-white">
							لیست اخبار
						</a>
					</li>
					<li class="breadcrumb-item text-white active" aria-current="page">
						{{$article->title}}
					</li>
				</ol>
			</nav>
		</div>
	</div>
</div>
<div class="article details py-5">
	<div class="container">
		<div class="row w-100 m-0">
			<div class="col-xl-3 col-md-4 d-md-block d-sm-none d-xs-none p-0">
				@include('site.article.content.art-side')
			</div>
			<div class="col-xl-9 col-md-8 p-1">
				@include('site.article.content.art-info')
			</div>
			<div class="col-xl-3 col-md-4 d-md-none d-sm-block d-xs-block p-0">
				@include('site.article.content.art-side')
			</div>
		</div>
	</div>
</div>

@stop