@extends('layouts.site.master')
@section('content')

<div class="inn-header position-relative">
	<img src="{{asset('assets/site/images/innheader.jpg')}}" class="w-100">
	<div class="overlay position-absolute bottom-0 end-0 start-0">
		<div class="container">
			<h1 class="fw-bolder text-white">
				لیست اخبار
			</h1>
			<nav class="p-1" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="{{url('/')}}" class="text-white">
							خانه
						</a>
					</li>
					<li class="breadcrumb-item text-white active" aria-current="page">
						لیست اخبار
					</li>
				</ol>
			</nav>
		</div>
	</div>
</div>
<div class="article list py-5">
	<div class="container">
		<div class="row w-100 m-0">
			@foreach($data as $row)
			<div class="col-xl-3 col-lg-4 col-sm-6 col-xs-12 p-1">
				<a href="{{url('/'.$row->url)}}" class="">
					<div class="card p-2 shadow-sm bg-light">
						<!-- cruper 1 * 1 -->
						<img src="{{asset('assets/uploads/medium/'.$row->image)}}" alt="" class="w-100">
						<h4 class="mb-0 mt-2 text-center text-dark">
							{{$row->title}}
						</h4>
					</div>
				</a>
			</div>
		    @endforeach
		</div>
	</div>
</div>

@stop