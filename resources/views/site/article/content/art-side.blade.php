<div class="side">
	<div class="row w-100 m-0">
		<div class="col-md-12 col-sm-6 p-1">
			<h4 class="m-0">
				اخبار مرتبط
			</h4>
		</div>
		@foreach($relate as $row)
		<div class="col-md-12 col-sm-6 p-1">
			<a href="">
				<div class="card bg-light shadow-sm">
					<div class="row w-100 m-0">
						<div class="col-sm-3 col-xs-3 align-self-center p-1">
							<img src="{{asset('assets/uploads/medium/'.$row->image)}}" alt="" class="w-100">
						</div>
						<div class="col-sm-9 col-xs-9 align-self-center p-1">
							<p class="m-0 text-dark">
								{{$row->title}}
							</p>
							<small class="text-secondary">
								{{jdate('d F Y',$row->created_at->timestamp)}}
							</small>
						</div>
					</div>
				</div>
			</a>
		</div>
		@endforeach

	</div>
</div>