<div class="info card bg-light shadow-sm p-3">
	<div class="row w-100 m-0">
		<div class="col-xxl-6 mx-auto p-1">
			<img src="{{asset('assets/site/images/arti.jpg')}}" alt="" class="imgart">
		</div>
		<div class="col-xxl-12 p-1">
			<h2 class="fw-bolder text-center my-2">
				{{$article->title}}
			</h2>
			<div class="desc">
				<p>
					{!! $article->description !!}
				</p>

			</div>
		</div>
	</div>
</div>