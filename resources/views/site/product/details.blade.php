@extends('layouts.site.master')
@section('content')

<div class="inn-header position-relative">
	<img src="{{asset('assets/site/images/innheader.jpg')}}" class="w-100">
	<div class="overlay position-absolute bottom-0 end-0 start-0">
		<div class="container">
			<h1 class="fw-bolder text-white">
				{{$product->title}}
			</h1>
			<nav class="p-1" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="{{url('/')}}" class="text-white">
							خانه
						</a>
					</li>
					<li class="breadcrumb-item">
						<a href="{{url('/'.@$product->category->url)}}" class="text-white">
							لیست محصولات
						</a>
					</li>
					<li class="breadcrumb-item text-white active" aria-current="page">
						{{$product->title}}
					</li>
				</ol>
			</nav>
		</div>
	</div>
</div>
<div class="product details py-5">
	<div class="container">
		<div class="row w-100 m-0">
			<div class="col-xl-4 col-lg-5 col-md-6 p-1">
				@include('site.product.content.pro-img')
			</div>
			<div class="col-xl-8 col-lg-7 col-md-6 p-1">
				@include('site.product.content.pro-info')
			</div>
		</div>
	</div>
</div>

@stop