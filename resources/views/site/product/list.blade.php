@extends('layouts.site.master')
@section('content')

<div class="inn-header position-relative">
	<img src="{{asset('assets/uploads/medium/'.$cat->image)}}" class="w-100">
	<div class="overlay position-absolute bottom-0 end-0 start-0">
		<div class="container">
			<h1 class="fw-bolder text-white">
		          لیست محصولات {{$cat->title}}
			</h1>
			<nav class="p-1" aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="{{url('/')}}" class="text-white">
							خانه
						</a>
					</li>
					<li class="breadcrumb-item text-white active" aria-current="page">
						لیست محصولات {{$cat->title}}
					</li>
				</ol>
			</nav>
		</div>
	</div>
</div>
<div class="product list py-5">
	<div class="container">
		<div class="row w-100 m-0">
			@foreach($products as $row)
			<div class="col-xxl-3 p-1">
				<a href="{{url('/'.$row->url)}}" class="cr">
					<img src="{{asset('assets/uploads/'.$row->image)}}" alt="{{$row->title}}" class="w-100">
					<h4 class="maxcontent mx-auto" style="background-color:{{$row->color}};">
						{{$row->title}}
					</h4>
				</a>
			</div>
		    @endforeach
		</div>
	</div>
</div>

@stop