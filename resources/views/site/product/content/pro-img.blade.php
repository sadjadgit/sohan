<div class="detimg bg-light shadow-sm position-relative p-3">

	@foreach($product->images as $key=>$row)
	<div class="mySlides">
		<div class="numbertext">{{$key++}} / {{$image_count}}</div>
		<img src="{{asset('assets/uploads/'.$row->image)}}" style="width:100%">
	</div>
    @endforeach

	<!-- <a class="prev" onclick="plusSlides(-1)">
		<i class="bi bi-chevron-left"></i>
	</a>
	<a class="next" onclick="plusSlides(1)">
		<i class="bi bi-chevron-right"></i>
	</a> -->

	<div class="row w-100 mx-0 mt-4">
		@foreach($product->images as $key=>$row)
		<div class="column p-1">
			<img class="demo cursor" src="{{asset('assets/uploads/'.$row->image)}}" onclick="currentSlide({{$key+1}})" />
		</div>
		@endforeach
	</div>


</div>