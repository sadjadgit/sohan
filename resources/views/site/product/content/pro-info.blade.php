<div class="detinfo bg-light shadow-sm p-3 h-100">
	<h2 class="fw-bolder">
		{{$product->title}}
	</h2>
	<div class="desc">
		<p>
	     {!! $product->description !!}
		</p>
	</div>
</div>