<!DOCTYPE html>
<html lang="en">

@include('layouts.site.blocks.head')

<body dir="rtl">
@include('layouts.site.blocks.menu')

<main class="content">
    @yield('content')
</main>

@include('layouts.site.blocks.footer')
<div class="btn-fix">
    <ul class="p-0 m-0">
        <li class="list-unstyled">
            <a href="tel:{{$setting->phone}}" class="rotate">
                <i class="bi bi-telephone d-flex"></i>
            </a>
        </li>
        <li class="list-unstyled">
            <a href="mailto:{{$setting->email}}">
                <i class="bi bi-envelope d-flex"></i>
            </a>
        </li>
    </ul>
</div>
<!-- Jquery, Popper, Saya, JS -->
@include('layouts.site.blocks.script')
</body>

</html>