<menu class="menu p-2 position-relative">
    <div class="row w-100 m-0">
        <div class="col-xxl-1 d-xl-block d-sm-none d-xs-none align-self-end p-1">

        </div>
        <div class="col-xxl-10 col-sm-4 col-xs-5 align-self-end text-center p-1">
            <a href="{{url('/')}}" class="">
                <img src="{{asset('assets/uploads/'.$setting->logo)}}" alt="{{$setting->title}}" class="py-2 logo">
            </a>
        </div>
        <div class="col-xxl-1 col-sm-8 col-xs-7 align-self-end p-1">
            <ul class="p-0 m-0">
                <li class="list-unstyled float-end ms-3">
						<span class="opennav bgone d-flex text-white p-1" onclick="openNav()">
							<i class="bi bi-list"></i>
						</span>
                </li>
                <li class="list-unstyled float-end ms-3">
                    <div class="searching">
                        <a href="javascript:void(0)"
                           class="search-open opennav bg-transparent d-flex textone p-1">
                            <i class="bi bi-search"></i>
                        </a>
                        <div class="search-inline">
                            <form>
                                <input type="text" class="form-control" placeholder="جستجو...">
                                <button type="submit">
                                    <i
                                        class="bi bi-search d-flex align-items-center justify-content-center"></i>
                                </button>
                                <a href="javascript:void(0)" class="search-close">
                                    <i class="bi bi-x d-flex"></i>
                                </a>
                            </form>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div id="mySidenav" class="sidenav">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">
            <i class="bi bi-x"></i>
        </a>
        <div class="">
            <a href="{{url('/')}}">
                خانه
            </a>
            <a href="">
                محصولات
            </a>
            <a href="{{url('/news')}}">
                اخبار
            </a>
            <a href="{{url('/about-us')}}">
                درباره ما
            </a>
            <a href="{{url('/contact-us')}}">
                تماس با ما
            </a>
        </div>
    </div>
</menu>