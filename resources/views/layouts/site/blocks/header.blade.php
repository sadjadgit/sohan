<header class="header position-relative">
    <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel" data-bs-interval="4000">
        <div class="carousel-indicators">
            @foreach($slider as $key=>$row)
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="{{$key}}"
                    class="shadow-sm border-0 @if($loop->first) active @endif" aria-current="true" aria-label="Slide {{$key++}}">

            </button>
            @endforeach

        </div>
        <div class="carousel-inner">
            @foreach($slider as $key=>$row)
            <div class="carousel-item @if($loop->first) active @endif">
                <!-- crop 3 * 1 -->
                <img src="{{asset('assets/uploads/medium/'.$row->image)}}" class="d-block w-100" alt="">
            </div>
            @endforeach
        </div>
    </div>
    <div class="overlay position-absolute">
        <h1 class="text-white text-end">
            <span class="one">
                3s means
            </span>
            <br>
            <span class="two">
                Act Different
            </span>
        </h1>
    </div>
</header>