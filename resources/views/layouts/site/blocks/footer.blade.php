<footer class="footer">
	<div class="container">
		<div class="row w-100 m-0">
			<div class="col-sm-12 p-1">
				<div class="footer-over">
					<div class="footer-over-one">
						<div class="footer-over-two">
							<img src="assets/site/images/b-4.jpg" alt="" class="">
							<div class="footer-over-sd">
								<h2 class="m-0">
									{{$setting->abouttitle}}
								</h2>
							</div>
							<img src="{{asset('assets/site/images/aaa.png')}}" class="img">
						</div>
					</div>
				</div>
			</div>
			<div class="col-xxl-2 col-sm-2 col-xs-6 px-2 py-3">
				<a href="{{url('/')}}" class="text-decoration-none">
					<img src="{{asset('assets/uploads/'.$setting->logo_footer)}}" alt="{{$setting->title}}"
						class="w-100 text-white">
				</a>
			</div>
			<div class="col-xxl-7 col-lg-7 col-sm-12 col-xs-12 p-0">
				<div class="row w-100 m-0">
					<div class="col-xl-8 col-sm-6 col-xs-12 px-2 py-3">
						<h4 class="fw-bolder text-white">
							آدرس
						</h4>
						<ul class="p-0 m-0">
							<li class="list-unstyled">
								<a class="text-white">
									واحد فروش : {{$setting->address}}

								</a>
							</li>
							<li class="list-unstyled">
								<a class="text-white">
									آدرس کارخانه : {{$setting->factory_address}}
								</a>
							</li>
							<li class="list-unstyled">
								<a class="text-white">
									کد پستی : {{$setting->postal_code}}
								</a>
							</li>
						</ul>
					</div>
					<div class="col-xl-4 col-sm-6 col-xs-12 px-2 py-3">
						<h4 class="fw-bolder text-white">
							تلفن
						</h4>
						<ul class="p-0 m-0">
							<li class="list-unstyled">
								<a class="text-white">
									واحد فروش :
									<a href="tel:{{$setting->contact}}" class="text-white">
										{{$setting->contact}}
									</a>
									<span class="text-white">-</span>
									<a href="tel:{{$setting->phone}}" class="text-white">
										{{$setting->phone}}
									</a>
								</a>
							</li>
							<li class="list-unstyled">
								<a class="text-decoration-none text-white">
									ایمیل :
									<a href="mailto:info@company.ir" class="text-white">
										{{$setting->email}}
									</a>
								</a>
							</li>
                            <li class="list-unstyled">
								<ul class="p-0 m-0">
                                    <li class="float-start list-unstyled me-2 mt-4">
                                        <a href="" class="bg-white d-flex p-1 rounded-3">
                                            <i class="bi bi-instagram h3 m-0 d-flex"></i>
                                        </a>
                                    </li>
                                    <li class="float-start list-unstyled me-2 mt-4">
                                        <a href="" class="bg-white d-flex p-1 rounded-3">
                                            <i class="bi bi-whatsapp h3 m-0 d-flex"></i>
                                        </a>
                                    </li>
                                    <li class="float-start list-unstyled me-2 mt-4">
                                        <a href="" class="bg-white d-flex p-1 rounded-3">
                                            <i class="bi bi-telegram h3 m-0 d-flex"></i>
                                        </a>
                                    </li>
                                </ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xxl-3 col-lg-3 col-md-5 col-sm-6 col-xs-12 px-2 py-3">
				{!! $setting->maps !!}
			</div>
		</div>
	</div>
</footer>