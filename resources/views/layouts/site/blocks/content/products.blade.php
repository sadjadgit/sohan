<div class="products py-xxl-2 py-xl-4 py-lg-3 py-md-2 py-sm-1 py-xs-2">
    <div class="container">
        <div class="col-xxl-7 col-xl-8 col-lg-9 col-md-10 col-sm-11 col-xs-12 px-1 pt-3 mx-auto text-center">
            <div class="red shadow-sm">
                <h2 class="fw-bolder text-white py-1 m-0 color-dd-gradient">
                    products
                </h2>
            </div>
        </div>
    </div>
    @foreach($category as $row)
        <div class="position-relative py-3 py-xl-4 py-lg-3 py-md-2 py-sm-1 py-xs-2">
            <div class="container">
                <div class="row w-100 m-0 gy-xxl-5 gy-xl-4 gy-lg-3 gy-md-2 gy-sm-1 gy-xs-4">
                    <div class="col-xxl-12 p-1">
                        <a href="{{url('/'.$row->url)}}">
                            <!-- crop 3 * 1 -->
                            <img src="{{asset('assets/uploads/medium/'.$row->image)}}" alt="{{$row->title}}" class="w-100">
                        </a>
                    </div>
                    <div class="col-xxl-12 p-1 m-0">
                        <div class="title @if($loop->iteration % 2 !== 0) title-start @else title-end   @endif position-absolute">
                            <h2>
                                {{$row->title}}
                            </h2>
                        </div>
                    </div>
                    <div class="col-xxl-12 p-1">
                        <div class="">
                            <section id="demos">
                                <div class="row">
                                    <div class="large-12 columns">
                                        <div class="owl-carousel owl-theme my-0">
                                            @foreach($row->childs as $item)
                                            <div class="item">
                                                <a href="{{url('/'.$item->url)}}">
                                                    <img src="{{asset('assets/uploads/'.$item->image)}}" alt=""
                                                         class="w-100">
                                                    <h4 class="maxcontent mx-auto"
                                                        style="background-color:{{$item->color}};">
                                                       {{$item->title}}
                                                    </h4>
                                                </a>
                                            </div>
                                                @endforeach
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>