<div class="services">
    <div class="container">
        <div class="row w-100 m-0 position-relative">
            <div class="col-xxl-12 p-1">
                <div class="title-serv">
                    <h2>
                        خدمات مشتریان
                    </h2>
                </div>
            </div>
            <div class="col-xxl-12 px-0 py-lg-5 py-md-2">
                <div class="row w-100 m-0">
                    <div class="col-xxl-8 col-lg-8 p-1">
                        <div class="row w-100 m-0">
                            @foreach($slogan as $row)
                            <div class="col-sm-4 p-md-3 p-sm-2 p-xs-1">
                                <div class="bg shadow-sm">
                                    <a href="#!">
                                       {{$row->title}}
                                    </a>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-xxl-4 col-lg-4 d-lg-block d-sm-none d-xs-none  p-1">
                        <img src="{{asset('assets/site/images/co.png')}}" class="img position-absolute">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>