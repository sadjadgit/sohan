<div class="news">
    <div class="container">
        <div class="row w-100 m-0">
            <div class="col-xxl-12 p-1">
                <div class="news-item position-relative">
                    <img src="{{asset('assets/site/images/b-3.jpg')}}" alt="" class="bg-news">
                    <div class="overlay">
                        <div class="row w-100 m-0 h-100">
                            <div
                                class="col-xxl-4 col-xl-5 col-lg-6 col-md-7 col-sm-8 col-xs-12 col-sd">
                                <div class="description">
                                    <h2 class="fw-bolder text-center">
                                      {{$article->title}}
                                    </h2>
                                    <div class="description-cap">
                                      {!! $article->description !!}
                                    </div>
                                    <a href="{{url('/'.$article->url)}}" class="">
                                        ادامه مطلب
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>