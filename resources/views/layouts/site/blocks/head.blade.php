<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="@yield('robots','noindex','nofollow')" />
    <link rel="shortcut icon" href="" type="image/x-icon">
    <title>
        {{$setting->title}}
    </title>
    <!-- Bootstrap Rtl, Saya, CSS -->
    <link rel="stylesheet" href="{{asset('assets/site/css/bootstrap.rtl.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/site/css/saya.css?v0.04')}}">
    <link rel="stylesheet" href="{{asset('assets/site/css/owlcarousel/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/site/css/owlcarousel/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/site/css/owlcarousel/docs.theme.min.css')}}">

</head>