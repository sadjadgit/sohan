<nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
        </li>
        <!-- <li class="nav-item d-none d-sm-inline-block">
            <a href="index3.html" class="nav-link">خانه</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="#" class="nav-link">تماس</a>
        </li> -->
    </ul>
    <!-- <form class="form-inline ml-3">
        <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar" type="search" placeholder="جستجو" aria-label="Search">
            <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                    <i class="fa fa-search"></i>
                </button>
            </div>
        </div>
    </form> -->

        <ul class="navbar-nav mr-auto">

{{--                <li class="nav-item dropdown">--}}
{{--                    <a class="nav-link" href="{{URL::action('Admin\CommentController@getIndex')}}">--}}
{{--                        <i class="fa fa-comments-o"></i>--}}
{{--                        <span class="badge badge-warning navbar-badge">@php $messages =App\Models\Message::wherereadat(0)->count(); echo$messages; @endphp</span>--}}
{{--                        کامنت ها--}}
{{--                    </a>--}}
{{--                </li>--}}


                <li class="nav-item">
                    <a class="nav-link" href="{{URL::action('Admin\ContactController@getIndex')}}">
                        <i class="fa fa-volume-control-phone"></i>
                        <span class="badge badge-warning navbar-badge">  @php $contacts =App\Models\Contact::wherereadat(Null)->count(); echo$contacts; @endphp </span>
                        تماس با ما
                    </a>
                </li>


        <!-- <li class="nav-item dropdown">
			<a class="nav-link" data-toggle="dropdown" href="#">
				<i class="fa fa-bell-o"></i>
				<span class="badge badge-warning navbar-badge">15</span>
			</a>
			<div class="dropdown-menu dropdown-menu-lg dropdown-menu-left">
				<span class="dropdown-item dropdown-header">15 نوتیفیکیشن</span>
				<div class="dropdown-divider"></div>
				<a href="#" class="dropdown-item">
					<i class="fa fa-envelope ml-2"></i> 4 پیام جدید
					<span class="float-left text-muted text-sm">3 دقیقه</span>
				</a>
				<div class="dropdown-divider"></div>
				<a href="#" class="dropdown-item">
					<i class="fa fa-users ml-2"></i> 8 درخواست دوستی
					<span class="float-left text-muted text-sm">12 ساعت</span>
				</a>
				<div class="dropdown-divider"></div>
				<a href="#" class="dropdown-item">
					<i class="fa fa-file ml-2"></i> 3 گزارش جدید
					<span class="float-left text-muted text-sm">2 روز</span>
				</a>
				<div class="dropdown-divider"></div>
				<a href="#" class="dropdown-item dropdown-footer">
                مشاهده همه نوتیفیکیشن
                </a>
			</div>
		</li> -->
            <li class="nav-item">
                <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#">
                    <i class="fa fa-th-large"></i>
                </a>
            </li>
        </ul>

</nav>
