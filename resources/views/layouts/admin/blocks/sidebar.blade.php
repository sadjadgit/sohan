<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="{{url('/admin')}}" class="brand-link">
        <img src="{{asset('assets/uploads/1.png')}}" alt="AdminLTE Logo" class="brand-image bg-white p-1">
        <span class="brand-text font-weight-light">پنل مدیریت</span>
    </a>
    <div class="sidebar" style="direction: ltr">
        <div style="direction: rtl">
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <li class="nav-item has-treeview menu" id="1">
                        <a href="{{url('/admin')}}" class="nav-link">
                            <i class="nav-icon fa fa-dashboard"></i>
                            <p>
                                داشبورد
                            </p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview menu" id="3">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-list"></i>
                            <p>
                                مدیریت محصولات
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview bg-white">

                            <li class="nav-item">
                                <a href="{{URL::action('Admin\CategoryController@getCategory')}}"
                                   class="nav-link text-dark">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>دسته بندی محصولات </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{URL::action('Admin\ProductController@getProduct')}}"
                                   class="nav-link text-dark">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>لیست محصولات </p>
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-item has-treeview menu" id="2">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-inbox"></i>
                            <p>
                                محتوا
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview bg-white">



                            <li class="nav-item">
                                <a href="{{URL::action('Admin\ContentController@getSlogan')}}"
                                   class="nav-link text-dark">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p> شعار صفحه اول </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{URL::action('Admin\WhyController@get')}}"
                                   class="nav-link text-dark">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p> چرا ما </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{URL::action('Admin\ArticleController@getArticle')}}"
                                    class="nav-link text-dark">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>مدیریت اخبار </p>
                                </a>
                            </li>


                            <li class="nav-item">
                                <a href="{{URL::action('Admin\SliderController@getSlider')}}"
                                    class="nav-link text-dark">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>اسلایدر </p>
                                </a>
                            </li>






                        </ul>
                    </li>
                    <li class="nav-item has-treeview menu" id="4">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-gear"></i>
                            <p>
                                تنظیمات
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview bg-white">

                            <li class="nav-item">
                                <a href="{{URL::action('Admin\SettingController@getEditSetting')}}"
                                    class="nav-link text-dark">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>تنظیمات سایت </p>
                                </a>
                            </li>


                            <li class="nav-item">
                                <a href="{{URL::action('Admin\SocialController@getSocial')}}"
                                    class="nav-link text-dark">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>تنظیمات شبکه اجتماعی </p>
                                </a>
                            </li>



                        </ul>
                    </li>
                    <li class="nav-item has-treeview menu" id="5">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-plus"></i>
                            <p>
                                افزونه های سایت
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview bg-white">

                            <li class="nav-item">
                                <a href="{{URL::action('Admin\ContentController@getUploader')}}"
                                    class="nav-link text-dark">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>آپلودر </p>
                                </a>
                            </li>


                            <li class="nav-item">
                                <a href="{{URL::action('Admin\ContentController@getCropper')}}"
                                    class="nav-link text-dark">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>کراپر </p>
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-item has-treeview menu" id="6">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-plus"></i>
                            <p>
                                افزونه های سئو
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview bg-white">



                            <li class="nav-item">
                                <a href="{{URL::action('Admin\RedirectController@getRedirect')}}"
                                    class="nav-link text-dark">
                                    <i class="fa fa-circle-o nav-icon"></i>
                                    <p>ریدایرکت </p>
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-item has-treeview menu" id="7">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fa fa-comment"></i>
                            <p>
                                پیام ها
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview bg-white">

                            <li class="nav-item">
                                <a href=""
                                    class="nav-link text-dark">
                                    <div class="float-right badge badge-danger position-absolute badgeee">
                                    </div>
                                    <i class="fa fa-circle-o nav-icon"></i> کامنت ها
                                </a>
                            </li>


                            <li class="nav-item">
                                <a href="{{URL::action('Admin\ContactController@getIndex')}}"
                                   class="nav-link text-dark">

                                    <div class="float-right badge badge-danger position-absolute badgeee">
                                        @php $contacts =App\Models\Contact::wherereadat(null)->count(); echo$contacts; @endphp
                                    </div>
                                    <i class="fa fa-circle-o nav-icon"></i> تماس با ما
                                </a>
                            </li>

                        </ul>
                    <li class="nav-item has-treeview menu" id="11">
                        <a href="{{url('/admin/logout')}}" class="nav-link">
                            <i class="nav-icon fa fa-power-off"></i>
                            <p>
                                خروج
                            </p>
                        </a>
                    </li>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</aside>
