<div class="container-fluid">
    @include('layouts.admin.blocks.card-box')
    <div class="row">
        <section class="col-lg-7 connectedSortable">
            {{--@include('layouts.admin.blocks.sales')--}}
            {{--@include('layouts.admin.blocks.conversation')--}}
            {{--@include('layouts.admin.blocks.to-do-list')--}}
        </section>
        <section class="col-lg-5 connectedSortable">
            {{--@include('layouts.admin.blocks.hits')--}}
            {{--@include('layouts.admin.blocks.sales-chart')--}}
            {{--@include('layouts.admin.blocks.calendar')--}}
        </section>
    </div>
</div>