<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title','پنل مدیریت')</title>
    <meta name="robots" content="@yield('robots','noindex')" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('assets/admin/plugins/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="{{asset('assets/admin/dist/css/adminlte.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/plugins/iCheck/flat/blue.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/plugins/morris/morris.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/plugins/datepicker/datepicker3.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/plugins/daterangepicker/daterangepicker-bs3.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/dist/css/bootstrap-rtl.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/dist/css/custom-style.css?v0.05')}}">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('assets/admin/css/toastr.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/toastr/toaster.css')}}">
    <!-- =============== cropper ===============-->
    <link rel="stylesheet" href="{{asset('assets/admin/cropper/css/cropper.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/cropper/css/main.css')}}">
    <script src="{{asset('assets/admin/plugins/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/jquery-ui.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.20.0/axios.min.js" integrity="sha512-quHCp3WbBNkwLfYUMd+KwBAgpVukJu5MncuQaWXgCrfgcxCJAq/fo+oqrRKOj+UKEmyMCG3tb8RB63W+EmrOBg==" crossorigin="anonymous"></script>
</head>
