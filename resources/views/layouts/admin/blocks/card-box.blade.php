<div class="row small-box-card">

	<div class="col-lg-3 col-6">
		<div class="small-box bg-custom-sd">
			<div class="inner">
				<h3></h3>
				<p>مدیریت محصولات</p>
			</div>
			<div class="icon">
				<i class="ion ion-bag"></i>
			</div>
			<a href="{{URL::action('Admin\ProductController@getProduct')}}" class="small-box-footer">
				اطلاعات بیشتر
				<i class="fa fa-arrow-circle-left"></i>
			</a>
		</div>
	</div>


	<div class="col-lg-3 col-6">
		<div class="small-box bg-custom-sd">
			<div class="inner">
				<h3></h3>
				<p>دسته بندی محصولات </p>
			</div>
			<div class="icon">
				<i class="ion ion-social-buffer-outline"></i>
			</div>
			<a href="{{URL::action('Admin\CategoryController@getCategory')}}" class="small-box-footer">
				اطلاعات بیشتر
				<i class="fa fa-arrow-circle-left"></i>
			</a>
		</div>
	</div>


	<div class="col-lg-3 col-6">
		<div class="small-box bg-custom-sd">
			<div class="inner">
				<h3></h3>
				<p>مدیریت مقاله </p>
			</div>
			<div class="icon">
				<i class="ion ion-ios-paper-outline"></i>
			</div>
			<a href="{{URL::action('Admin\ArticleController@getArticle')}}" class="small-box-footer">
				اطلاعات بیشتر
				<i class="fa fa-arrow-circle-left"></i>
			</a>
		</div>
	</div>


	<div class="col-lg-3 col-6">
		<div class="small-box bg-custom-sd">
			<div class="inner">
				<h3></h3>
				<p> اسلایدر</p>
			</div>
			<div class="icon">
				<i class="ion ion-social-windows-outline"></i>
			</div>
			<a href="{{URL::action('Admin\SliderController@getSlider')}}" class="small-box-footer">
				اطلاعات بیشتر
				<i class="fa fa-arrow-circle-left"></i>
			</a>
		</div>
	</div>


	<div class="col-lg-3 col-6">
		<div class="small-box bg-custom-sd">
			<div class="inner">
				<h3></h3>
				<p> شعار صفحه اول</p>
			</div>
			<div class="icon">
				<i class="ion ion-android-clipboard"></i>
			</div>
			<a href="{{URL::action('Admin\SampleController@getSample')}}" class="small-box-footer">
				اطلاعات بیشتر
				<i class="fa fa-arrow-circle-left"></i>
			</a>
		</div>
	</div>
	<div class="col-lg-3 col-6">
		<div class="small-box bg-custom-sd">
			<div class="inner">
				<h3></h3>
				<p> چرا ما</p>
			</div>
			<div class="icon">
				<i class="ion ion-android-clipboard"></i>
			</div>
			<a href="{{URL::action('Admin\WhyController@get')}}" class="small-box-footer">
				اطلاعات بیشتر
				<i class="fa fa-arrow-circle-left"></i>
			</a>
		</div>
	</div>



	<div class="col-lg-3 col-6">
		<div class="small-box bg-custom-sd">
			<div class="inner">
				<h3></h3>
				<p>تنظیمات سایت </p>
			</div>
			<div class="icon">
				<i class="ion ion-ios-gear-outline"></i>
			</div>
			<a href="{{URL::action('Admin\SettingController@getEditSetting')}}" class="small-box-footer">
				اطلاعات بیشتر
				<i class="fa fa-arrow-circle-left"></i>
			</a>
		</div>
	</div>


	<div class="col-lg-3 col-6">
		<div class="small-box bg-custom-sd">
			<div class="inner">
				<h3></h3>
				<p>تنظیمات شبکه اجتماعی </p>
			</div>
			<div class="icon">
				<i class="ion ion-social-instagram-outline"></i>
			</div>
			<a href="{{URL::action('Admin\SocialController@getSocial')}}" class="small-box-footer">
				اطلاعات بیشتر
				<i class="fa fa-arrow-circle-left"></i>
			</a>
		</div>
	</div>



	<div class="col-lg-3 col-6">
		<div class="small-box bg-custom-sd">
			<div class="inner">
				<h3></h3>
				<p>ریدایرکت </p>
			</div>
			<div class="icon">
				<i class="ion ion-arrow-return-right"></i>
			</div>
			<a href="{{URL::action('Admin\RedirectController@getRedirect')}}" class="small-box-footer">
				اطلاعات بیشتر
				<i class="fa fa-arrow-circle-left"></i>
			</a>
		</div>
	</div>


	<div class="col-lg-3 col-6">
		<div class="small-box bg-custom-sd">
			<div class="inner">
				<h3></h3>
				<p> تماس با ما </p>
			</div>
			<div class="icon">
				<i class="ion ion-ios-telephone-outline"></i>
			</div>
			<a href="{{URL::action('Admin\ContactController@getIndex')}}" class="small-box-footer">
				اطلاعات بیشتر
				<i class="fa fa-arrow-circle-left"></i>
			</a>
		</div>
	</div>

</div>
