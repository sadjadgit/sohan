
<script>
$.widget.bridge('uibutton', $.ui.button)
</script>
<script src="{{asset('assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/adminplugins/raphael-min.js')}}"></script>
<script src="{{asset('assets/admin/plugins/morris/morris.min.js')}}"></script>
<script src="{{asset('assets/admin/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('assets/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<script src="{{asset('assets/admin/plugins/knob/jquery.knob.js')}}"></script>
<script src="{{asset('assets/admin/plugins/moment.min.js')}}"></script>
<script src="{{asset('assets/admin/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('assets/admin/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<script src="{{asset('assets/admin/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('assets/admin/plugins/fastclick/fastclick.js')}}"></script>
<script src="{{asset('assets/admin/dist/js/adminlte.js')}}"></script>
<script src="{{asset('assets/admin/dist/js/pages/dashboard.js')}}"></script>
<script src="{{asset('assets/admin/dist/js/demo.js')}}"></script>
<script src="{{asset('assets/admin/js/toastr.js')}}"></script>
<script src="{{asset('assets/admin/toastr/toastr.js')}}"></script>
<!-- =============== ckeditor SCRIPTS ===============-->
<script src="{{asset('assets/admin/ckeditor/ckeditor.js')}}"></script>
<!-- =============== cropper ===============-->
<script src="{{asset('assets/admin/cropper/js/cropper.js')}}"></script>
<script src="{{asset('assets/admin/cropper/js/main.js?v0.13')}}"></script>
<!--vue-->
@yield('js')
