<!DOCTYPE html>
<html lang="en">
@include('layouts.admin.blocks.head')

<body class="hold-transition sidebar-mini">
    <div class="wrapper" id="app34564565">
        @include('layouts.admin.blocks.navbar')
        @include('layouts.admin.blocks.sidebar')
        <div class="content-wrapper">
            <div class="content-header">
                <div class="container-fluid">
                </div>
            </div>
            <section class="content">
                @yield('content')
            </section>
        </div>
        @include('layouts.admin.blocks.footer')
    </div>
    @include('layouts.admin.blocks.script')
    @include('layouts.admin.blocks.message')
</body>

</html>
