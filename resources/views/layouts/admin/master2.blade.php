<!DOCTYPE html>
<html>
@include('layouts.admin.blocks.head')

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        @include('layouts.admin.blocks.navbar')
        <section class="content">
            @yield('content')
        </section>
        @include('layouts.admin.blocks.footer')
    </div>
    @include('layouts.admin.blocks.script')
    @include('layouts.admin.blocks.message')
</body>

</html>