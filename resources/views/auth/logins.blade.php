<!DOCTYPE html>
<html>
@include('layouts.admin.blocks.head')
<style>
.login {
	height: 100vh;
	display: flex;
	align-items: center;
	justify-content: center;
}

.login .row {
	width: 100%;
	margin: 0px
}

.login .card {
	border-radius: 3vh;
}

.login .card .pas {
	letter-spacing: 2vh;
	text-align: center
}

.login .card input,
.login .card button {
	border-radius: 2vh;
}
</style>

<body class="hold-transition sidebar-mini">
	<div class="login bg-light">
		<div class="row">
			<div class="col-md-3 p-0 mx-auto">
				<div class="card p-3 border-0 shadow-lg">
					<form class="m-0" method="post" action="{{URL::action('Admin\LoginController@postLogin')}}"
						enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="form-group m-2">
							<h3 class="text-center my-0 pb-3">
								ورود
							</h3>
						</div>
						<div class="form-group m-2">
							<label for="" class="text-secondary font-weight-light">ایمیل را وارد کنید :</label>
							<input type="email" class="form-control shadow-sm p-3" name="email">
						</div>
						<div class="form-group m-2">
							<label for="" class="text-secondary font-weight-light">رمز عبور :</label>
							<input type="password" class="form-control shadow-sm p-3 pas" name="password" laceholder="- - - - -">
						</div>
						<div class="form-group mt-4 mx-2">
							<button type="submit" class="btn btn-lg btn-success btn-block shadow-sm rounded-lg">
								ورود
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.admin.blocks.script')
</body>

</html>