<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->string('logo')->nullable();
            $table->string('favicon')->nullable();
            $table->string('abouttitle')->nullable();
            $table->longText('about')->nullable();
            $table->longText('footer_about')->nullable();
            $table->longText('about2')->nullable();
            $table->longText('rules')->nullable();
            $table->string('aboutimg')->nullable();
            $table->longText('contact')->nullable();
            $table->longText('phone')->nullable();
            $table->longText('maps')->nullable();
            $table->longText('email')->nullable();
            $table->longText('address')->nullable();
            $table->longText('description_seo')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting');
    }
}
