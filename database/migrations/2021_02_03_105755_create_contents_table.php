<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->string('title_seo')->nullable();
            $table->string('img_seo')->nullable();
            $table->string('image')->nullable();
            $table->longText('description')->nullable();
            $table->longText('lead')->nullable();
            $table->longText('description_seo')->nullable();
            $table->longText('url')->nullable();
            $table->boolean('status')->nullable();
            $table->string('type')->nullable();
            $table->bigInteger('parent_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}
