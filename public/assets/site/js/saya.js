// search
var sp = document.querySelector('.search-open');
var searchbar = document.querySelector('.search-inline');
var shclose = document.querySelector('.search-close');
function changeClass() {
    searchbar.classList.add('search-visible');
}
function closesearch() {
    searchbar.classList.remove('search-visible');
}
sp.addEventListener('click', changeClass);
shclose.addEventListener('click', closesearch);

// owl carousel
$(document).ready(function () {
	var owl = $(".owl-carousel");
	owl.owlCarousel({
		autoplayHoverPause: true,
		responsiveClass: true,
		autoplay: true,
		dots: false,
		loop: true,
		rtl: true,
		margin: 0,
		responsive: {
			0: {
				items: 1.5,
				nav: true,
			},
			576: {
				items: 2.5,
				nav: true,
			},
			768: {
				items: 3,
				nav: true,
			},
			1200: {
				items: 4,
				nav: false,
			},
			1400: {
				items: 4,
				nav: false,
			},
		},
	});
	$(".play").on("click", function () {
		owl.trigger("play.owl.autoplay", [250]);
	});
	$(".stop").on("click", function () {
		owl.trigger("stop.owl.autoplay");
	});
});

// baner owl carousel
$(document).ready(function () {
	var owl = $(".owl-carousel-blog");
	owl.owlCarousel({
		autoplayHoverPause: true,
		responsiveClass: true,
		autoplay: true,
		dots: false,
		nav: false,
		rtl: true,
		margin: 0,
		responsive: {
			0: {
				items: 1.5,
				loop: true,
			},
			576: {
				items: 2.5,
				loop: true,
			},
			768: {
				items: 3,
				loop: true,
			},
			1200: {
				items: 3,
				loop: true,
			},
			1400: {
				items: 4,
				loop: false,
			},
		},
	});
	$(".play").on("click", function () {
		owl.trigger("play.owl.autoplay", [250]);
	});
	$(".stop").on("click", function () {
		owl.trigger("stop.owl.autoplay");
	});
});

// brand owl carousel
$(document).ready(function () {
	var owl = $(".owl-carousel-brand");
	owl.owlCarousel({
		autoplayHoverPause: true,
		responsiveClass: true,
		autoplay: true,
		dots: false,
		nav: false,
		rtl: true,
		margin: 0,
		responsive: {
			0: {
				items: 2.5,
				loop: true,
			},
			576: {
				items: 3.5,
				loop: true,
			},
			768: {
				items: 4,
				loop: true,
			},
			1200: {
				items: 6,
				loop: true,
			},
			1400: {
				items: 8,
				loop: false,
			},
		},
	});
	$(".play").on("click", function () {
		owl.trigger("play.owl.autoplay", [250]);
	});
	$(".stop").on("click", function () {
		owl.trigger("stop.owl.autoplay");
	});
});

// side nav
function openNav() {
	document.getElementById("mySidenav").style.width = "100%";
}

function closeNav() {
	document.getElementById("mySidenav").style.width = "0";
}

// showSlides
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}