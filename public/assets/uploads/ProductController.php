<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\StoreUserRequest;
use App\Models\Content;
use App\Models\Product;
use App\Models\Image;
use Classes\UploadImg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class ProductController extends Controller
{
//    /**
//     * Create a new controller instance.
//     *
//     * @return void
//     */
//    public function __construct()
//    {
//        $this->middleware('auth:admin');
//    }
//
//    /**
//     * Show the application dashboard.
//     *
//     * @return \Illuminate\Contracts\Support\Renderable
//     */
    public function getProduct()
    {
        $products = Content::orderBy('id','DESC')->Product()->paginate(100);
        $cat = Content::Cat()->get();

        return View('admin.products.index')

            ->with('products', $products)

            ->with('category', $cat);
    }

    public function getAddProduct()
    {

        $cat = Content::Cat()->get();
        return View('admin.products.add')
            ->with('category', $cat);


    }

    public function postAddProduct(ProductRequest $request)
    {
        $input = $request->all();
        $input['status'] = $request->has('status');
        $input['url']=str_replace(' ', '-',$input['url']);
//        if ($request->hasFile('image')) {
//            $path = "assets/uploads/";
//            $uploader = new UploadImg();
//            $fileName = $uploader->uploadPic($request->file('image'), $path);
//            if($fileName){
//                $input['image'] = $fileName;
//            }else{
//                return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
//            }
//        }
        if ($request->hasFile('image')) {
            $pathMain = "assets/uploads/";
            $extension = $request->file('image')->getClientOriginalName();
            $fileName = random_int(1000 , 9999). "$extension";
            $request->file('image')->move($pathMain, $fileName);
            $input['image'] = $fileName;
        }
        $input['type'] = 'product';
        $product = Content::create($input);
        return Redirect::action('Admin\ProductController@getProduct');
    }

    public function getEditProduct($id)
    {
        $cat = Content::Cat()->get();
        $data = Content::orderBy('id','DESC')->Product()->findorfail($id);
        return View('admin.products.edit')
            ->with('data', $data)
            ->with('category', $cat);
    }

    public function postEditProduct($id, ProductRequest $request)
    {
        $input = $request->all();
        $input['status'] = $request->has('status');
        $input['url']=str_replace(' ', '-',$input['url']);
        $content = Content::find($id);
//        if ($request->hasFile('image')) {
//            $path = "assets/uploads/";
//            File::delete($path . '/big/' . $content->image);
//            File::delete($path . '/medium/' . $content->image);
//            File::delete($path . '/small/' . $content->image);
//            $uploader = new UploadImg();
//            $fileName = $uploader->uploadPic($request->file('image'), $path);
//            if($fileName){
//                $input['image'] = $fileName;
//            }else{
//                return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
//            }
//        }
//        else {
//            $input['image'] = $content->image;
//        }
        if ($request->hasFile('image')) {
            \Illuminate\Support\Facades\File::delete("assets/uploads/".$content->image);
            $pathMain = "assets/uploads/";
            $extension = $request->file('image')->getClientOriginalName();
            $fileName = random_int(1000 , 9999). "$extension";
            $request->file('image')->move($pathMain, $fileName);
            $input['image'] = $fileName;
        }
        else
        {
            $input['image'] = $content->image ;
        }

        $product = Content::orderBy('id','DESC')->Product()->find($id);
        $input['type'] = 'product';
        $product->update($input);
        return Redirect::action('Admin\ProductController@getProduct');
    }
    public function getDeleteProduct($id)
    {

        $content = Content::find($id);
        File::delete('assets/uploads/content/pro/small/' . $content->image);
        File::delete('assets/uploads/content/pro/big/' . $content->image);
        File::delete('assets/uploads/content/pro/medium/' . $content->image);
        Content::destroy($id);
        return Redirect::action('Admin\ProductController@getProduct');

    }
    public function postDeleteProduct(ProductRequest $request)
    {
        $images = Content::whereIn('id', $request->get('deleteId'))->Product()->pluck('image');
        foreach ($images as $item) {
            File::delete('assets/uploads/content/pro/small/' . $item);
            File::delete('assets/uploads/content/pro/big/' . $item);
            File::delete('assets/uploads/content/pro/medium/' . $item);
        }
        if (Content::destroy($request->get('deleteId'))) {
            return Redirect::back()
                ->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }

    }
//========================== PRODUCT IMAGE =================================================

    public function getImage($id)
    {
        $image = Content::find($id);
        $data = Image:: whereContentId($image->id)->paginate(40);
        return view('admin.product_image.index')
            ->with('data',$data)
            ->with('image', $image);
    }

    public function getAddImage($product_id)
    {
        $image = Content::find($product_id);
        $data = Image:: whereContentId($image->id)->paginate(40);
        return view('admin.product_image.add')
            ->with('data',$data)
            ->with('image', $image)
            ->with('product_id', $product_id);
    }

    public function postAddImage(Request $request)
    {
        $input = $request->all();
        if ($request->hasFile('image')) {
            $path = "assets/uploads/content/pro/";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image'), $path);
            if($fileName){
                $input['image'] = $fileName;
            }else{
                return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
            }
        }
        $status = Image::create($input);
        return \redirect('/admin/products/image/'.$input['content_id'])->with('success','آیتم موردنظر با موفقیت ثبت شد.');
    }

    public function getEditImage($product_id)
    {
        $data = Image::find($product_id);

        return view('admin.product_image.edit')
            ->with('data' , $data)
            ->with('product_id', $product_id);
    }

    public function postEditImage($id , Request $request)
    {
        $input = $request->all();
        $image = Image::find($id);
        if ($request->hasFile('image')) {
            $path = "assets/uploads/content/pro/";
            File::delete($path . '/big/' . $image->image);
            File::delete($path . '/medium/' . $image->image);
            File::delete($path . '/small/' . $image->image);
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image'), $path);
            if($fileName){
                $input['image'] = $fileName;
            }else{
                return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
            }
        }
        else {
            $input['image'] = $image->image;
        }
        $input['content_id'] = $image->product_id;
        $image->update($input);
        return \redirect('/admin/products/image/'.$image->product_id)->with('success','آیتم موردنظر با موفقیت ویرایش شد.');
    }

    public function postDeleteImage(Request $request)
    {
        $images = Image::whereIn('id', $request->get('deleteId'))->pluck('image');
        foreach ($images as $item) {
            File::delete('assets/uploads/content/pro/small/' . $item);
            File::delete('assets/uploads/content/pro/big/' . $item);
            File::delete('assets/uploads/content/pro/medium/' . $item);
        }
        if (Image::destroy($request->get('deleteId'))) {
            return \redirect()->back()->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }
    }


}
