<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

//            'email' => ['required',  Rule::unique('users')],
            'url' => ['',  'unique:contents,url'],
//            'password' => ['required','min:6','confirmed']

        ];
    }
    public function massages()
    {
        return [
            'url.unique'=>'لینک تکراریست'
            ,''=>'لینک اجباریست'
        ];
    }
}
