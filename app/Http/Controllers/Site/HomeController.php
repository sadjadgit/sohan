<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Content;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;


class HomeController extends Controller
{

    public function getIndex()
    {
        $product = Content::orderBy('id','DESC')->product()->whereStatus(1)->get();
        $category = Content::orderBy('id','ASC')->cat()->whereStatus(1)->get();
        $slogan = Content::orderBy('id','DESC')->slogan()->take(3)->get();
        $slider = Content::orderBy('id','ASC')->slider()->whereStatus(1)->get();
        $article = Content::orderBy('id','DESC')->article()->whereStatus(1)->first();
         return view('site.index')
                   ->with('product',$product)
                  ->with('slogan',$slogan)
                  ->with('slider',$slider)
                  ->with('article',$article)
                   ->with('category',$category);
    }

    public function getContact()
    {
        return view('site.contactus');
    }

    public function postContact(Request $request)
    {
        $input = $request->all();
        $input['readat'] = 0;
        $contact = Contact::create($input);
        return Redirect::action('Site\HomeController@getContact')->with('success','با موفقیت ثبت شد');
    }

    public function getAbout()
    {
        return view('site.aboutus');
    }


     public function getSearch(Request $request)
    {

        $news = [];

        $news = Content::article()->where(function ($query){
            if (Input::has('search') )
            {
                $query->where('title', 'LIKE', '%' . Input::get('search') . '%')
                    ->orWhere('description', 'LIKE', '%' . Input::get('search') . '%');
            }
        })->get();

        $product = [];
        $product = Content::product()->where(function ($query){
            if (Input::has('search') )
            {
                $query->where('title', 'LIKE', '%' . Input::get('search') . '%')
                    ->orWhere('description', 'LIKE', '%' . Input::get('search') . '%');
            }
        })->get();

        $category = [];
        $category = Content::cat()->where(function ($query){
            if (Input::has('search') )
            {
                $query->where('title', 'LIKE', '%' . Input::get('search') . '%')
                    ->orWhere('description', 'LIKE', '%' . Input::get('search') . '%');
            }
        })->get();


        $a = [];

        $count  = count($news) + count($product) + count($category);


        if($news !== [] and $product !==[] and $category !==[]) {
            if ($category != null || $product != null || $news != null)
            {
                $a = array_merge($category->toArray(), $product->toArray() , $news->toArray());
            }
        }

        $all= $this->paginate($a,5);
        $search = Input::get('search');
        return view('site.search')
            ->with('all',$all)
            ->with('count',$count)
            ->with('product',$product)
            ->with('category',$category)
            ->with('news',$news)
            ->with('search',$search);
    }



}
