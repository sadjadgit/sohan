<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Content;
use App\Models\RelateData;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    public function getArticleList()
    {
        $data = Content::article()->orderBy('id','DESC')->get();
        return view('site.article.list')
                  ->with('data',$data);
    }

    public function getArticleDetails(Request $request)
    {
        $seg = $request->segments();
        $article = Content::article()->where('url',$seg[0])->first();
        $rel = RelateData::where('relatable_id',$article->id)->where('relatable_type',"App\Models\Content")->orderBy('id','DESC')->pluck('datable_id');
        $relate = Content::whereIn('id',$rel)->orderBy('id','DESC')->get();
        return view('site.article.details')
                  ->with('article',$article)
                  ->with('relate',$relate);
    }
}
