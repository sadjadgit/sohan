<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Content;
use App\Models\Image;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function getProductList(Request $request)
    {
        $seg = $request->segments();
        $cat = Content::cat()->where('url',$seg[0])->first();
        $products = Content::where('parent_id',$cat->id)->orderBy('id','DESC')->get();
        return view('site.product.list')
               ->with('cat',$cat)
               ->with('products',$products);
    }

    public function getProductDetails(Request $request)
    {
        $seg = $request->segments();
        $product = Content::product()->where('url',$seg[0])->first();
        $image_count = Image::where('content_id',$product->id)->count();
        return view('site.product.details')
               ->with('product',$product)
               ->with('image_count',$image_count);
    }
}
