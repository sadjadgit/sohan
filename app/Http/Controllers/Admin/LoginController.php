<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function getLogin()
    {
        return view('auth.logins');

    }
    public function postLogin(LoginRequest $request)
    {
        $input = $request->all();
        $login = Auth::attempt([
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'admin' => true,
        ]);

        if ($login) {
            return redirect('/admin');
        } else {
            return \redirect()->back()->with('error', '');
        }
    }
}
