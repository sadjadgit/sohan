<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Models\Content;
use Classes\UploadImg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class CategoryController extends Controller
{


    public function getCategory()
    {
        $category = Content::orderBy('id','DESC')->Cat()->paginate(100);


        return View('admin.category.index')
            ->with('category', $category);
    }

    public function getAddCategory()
    {


        return View('admin.category.add');

    }

    public function postAddCategory(ProductRequest $request)
    {
        $input = $request->all();
        $input['status'] = $request->has('status');
        $input['url']=str_replace(' ', '-',$input['url']);
//        if ($request->hasFile('image')) {
//            $path = "assets/uploads/";
//            $uploader = new UploadImg();
//            $fileName = $uploader->uploadPic($request->file('image'), $path);
//            if($fileName){
//                $input['image'] = $fileName;
//            }else{
//                return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
//            }
//        }

        if ($request->get('image_get')) {
            $fileName = mt_rand(100, 999) . ".jpg";
            file_put_contents("assets/uploads/medium/" . $fileName, file_get_contents($input['image_get']));
            file_put_contents("assets/uploads/small/" . $fileName, file_get_contents($input['image_get']));
            file_put_contents("assets/uploads/big/" . $fileName, file_get_contents($input['image_get']));
            @$input['image'] = $fileName;
        }

        $input['type'] = 'cat';

        $category = Content::create($input);
        return Redirect::action('Admin\CategoryController@getCategory');
    }

    public function getEditCategory($id)
    {

        $data = Content::findOrfail($id);
        return View('admin.category.edit')
            ->with('data', $data);
    }

    public function postEditCategory($id, ProductRequest $request)
    {
        $input = $request->all();
        $input['status'] = $request->has('status');
        $input['url']=str_replace(' ', '-',$input['url']);
        $content = Content::find($id);
//        if ($request->hasFile('image')) {
//            $path = "assets/uploads/";
//            File::delete($path . '/big/' . $content->image);
//            File::delete($path . '/medium/' . $content->image);
//            File::delete($path . '/small/' . $content->image);
//            $uploader = new UploadImg();
//            $fileName = $uploader->uploadPic($request->file('image'), $path);
//            if($fileName){
//                $input['image'] = $fileName;
//            }else{
//                return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
//            }
//        }
//        else {
//            $input['image'] = $content->image;
//        }
        if ($request->get('image_get')) {
            $path = "assets/admin/content/article/";
            File::delete($path . '/big/' . $content->image);
            File::delete($path . '/medium/' . $content->image);
            File::delete($path . '/small/' . $content->image);
            $fileName = mt_rand(100, 999) . ".jpg";
            file_put_contents("assets/uploads/medium/" . $fileName, file_get_contents($input['image_get']));
            file_put_contents("assets/uploads/small/" . $fileName, file_get_contents($input['image_get']));
            file_put_contents("assets/uploads/big/" . $fileName, file_get_contents($input['image_get']));
            @$input['image'] = $fileName;
        }
        else
        {
            $input['image'] = $content->image;
        }


        $product = Content::orderBy('id','DESC')->Cat()->find($id);
        $input['type'] = 'cat';

        $product->update($input);
        return Redirect::action('Admin\CategoryController@getCategory');
    }
    public function getDeleteCategory($id)
    {

        $content = Content::find($id);
        File::delete('assets/uploads/content/cat/small/' . $content->image);
        File::delete('assets/uploads/content/cat/big/' . $content->image);
        File::delete('assets/uploads/content/cat/medium/' . $content->image);
        Content::destroy($id);
        return Redirect::action('Admin\CategoryController@getCategory');

    }

    public function postDeleteCategory(ProductRequest $request)
    {
        $images = Content::whereIn('id', $request->get('deleteId'))->Cat()->pluck('image');
        foreach ($images as $item) {
            File::delete('assets/uploads/content/cat/small/' . $item);
            File::delete('assets/uploads/content/cat/big/' . $item);
            File::delete('assets/uploads/content/cat/medium/' . $item);
        }

        if (Content::destroy($request->get('deleteId'))) {
            return Redirect::back()
                ->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }
    }
    public function postSort(ProductRequest $request)
    {

        if ($request->get('update') == "update") {
            $count = 1;
            if ($request->get('update') == 'update') {
                foreach ($request->get('arrayorder') as $idval) {

                    $category = Content::Cat()->find($idval);
                    $category->listorder = $count;
                    $category->save();
                    $count++;
                }
                echo 'با موفقیت ذخیره شد.';
            }
        }
    }








}
