<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;

use App\Models\Content;
use App\User;
use Classes\UploadImg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class ContentController extends Controller
{
    public function getSlogan()
    {
        $slogan = Content::orderBy('id','DESC')->slogan()->paginate(100);
        return View('admin.slogan.index')
            ->with('slogan', $slogan);
    }

    public function getAddSlogan()
    {
        return View('admin.slogan.add');
    }

    public function postAddSlogan(Request $request)
    {
        $input = $request->all();

        if ($request->hasFile('image')) {
            $pathMain = "assets/uploads/";
            $extensionf = $request->file('image')->getClientOriginalName();
            $fileName = mt_rand(100, 999)."$extensionf";
            $request->file('image')->move($pathMain, $fileName);
            $input['image'] = $fileName;
        }
        $input['type'] = 'slogan';
        $Sample = Content::create($input);
        return Redirect::action('Admin\ContentController@getSlogan');
    }

    public function getEditSlogan($id)
    {
        $data = Content::orderBy('id','DESC')->Sample()->findorfail($id);
        return View('admin.slogan.edit')
            ->with('data', $data);
    }

    public function postEditSlogan($id, Request $request)
    {
        $input = $request->all();
        $content = Content::find($id);
        if ($request->hasFile('image')) {
            File::delete('assets/uploads/' . $content->image);
            $pathMain = "assets/uploads/";
            $extensionf = $request->file('image')->getClientOriginalName();
            if (true) {
                $fileName = mt_rand(100, 999)."$extensionf";
                $request->file('image')->move($pathMain, $fileName);
                $input['image'] = $fileName;
            } else {
                return Redirect::back()->with('eror', 'فایل ارسالی صحیح نیست.');
            }
        } else {
            $input['image'] = $content->image;
        }
        $Sample = Content::orderBy('id','DESC')->Slogan()->find($id);
        $Sample->update($input);
        return Redirect::action('Admin\ContentController@getSlogan');
    }
    public function getDeleteSlogan($id)
    {

        $content = Content::find($id);
        File::delete('assets/uploads/small/' . $content->image);
        File::delete('assets/uploads/big/' . $content->image);
        File::delete('assets/uploads/medium/' . $content->image);
        Content::destroy($id);
        return Redirect::action('Admin\ContentController@getSlogan');

    }
    public function postDeleteSlogan(Request $request)
    {
        $images = Content::whereIn('id', $request->get('deleteId'))->Sample()->pluck('image');
        foreach ($images as $item) {
            File::delete('assets/uploads/small/' . $item);
            File::delete('assets/uploads/big/' . $item);
            File::delete('assets/uploads/medium/' . $item);
        }
        if (Content::destroy($request->get('deleteId'))) {
            return Redirect::back()
                ->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }

    }



    public function getAdmin()
    {
        return View('admin.index');
    }


    public function getCropper(){
        return View('admin.cropper');

    }

    public function getUploader()
    {
        $uploader = \App\Models\Content::Upl()->get();
        return View('admin.uploader.index')
            ->with('uploader', $uploader);
    }

    public function getAddUploader()
    {
        return View('admin.uploader.add');

    }

    public function postAddUploader(Request $request)
    {
        set_time_limit(10000);
        $input = $request->all();
//        $input['url']=str_replace(' ', '-',$input['url']);
        if ($request->hasFile('image')) {
            $path = "assets/uploads/content/upl/";
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image'), $path);
            if($fileName){
                $input['image'] = $fileName;
            }else{
                return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
            }
        }
        $input['type'] = 'upl';
        $uploader = Content::create($input);
        return Redirect::action('Admin\ContentController@getUploader');
    }

    public function getEditUploader($id)
    {
        $data = Content::Upl()->findorfail($id);
        return View('admin.uploader.edit')
            ->with('data', $data);
    }

    public function postEditUploader($id, Request $request)
    {
        set_time_limit(10000);
        $input = $request->all();
//        $input['url']=str_replace(' ', '-',$input['url']);
        $content = Content::find($id);
        if ($request->hasFile('image')) {
            $path = "assets/uploads/content/upl/";
            File::delete($path . '/big/' . $content->image);
            File::delete($path . '/medium/' . $content->image);
            File::delete($path . '/small/' . $content->image);
            $uploader = new UploadImg();
            $fileName = $uploader->uploadPic($request->file('image'), $path);
            if($fileName){
                $input['image'] = $fileName;
            }else{
                return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
            }
        }
        else {
            $input['image'] = $content->image;
        }

        $content->update($input);
        return Redirect::action('Admin\ContentController@getUploader');
    }
    public function getDeleteUploader($id)
    {

        $content = Content::find($id);
        File::delete('assets/uploads/content/upl/small/' . $content->image);
        File::delete('assets/uploads/content/upl/big/' . $content->image);
        File::delete('assets/uploads/content/upl/medium/' . $content->image);
        Content::destroy($id);
        return Redirect::action('Admin\ContentController@getUploader');

    }
}
