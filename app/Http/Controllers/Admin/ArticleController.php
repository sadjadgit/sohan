<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\StoreUserRequest;
use App\Models\Content;
use App\Models\RelateData;
use Classes\UploadImg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class ArticleController extends Controller
{
//    /**
//     * Create a new controller instance.
//     *
//     * @return void
//     */
//    public function __construct()
//    {
//        $this->middleware('auth:admin');
//    }
//
//    /**
//     * Show the application dashboard.
//     *
//     * @return \Illuminate\Contracts\Support\Renderable
//     */
    public function getArticle()
    {
        $articles = Content::orderBy('id','DESC')->Article()->paginate(100);

        return View('admin.article.index')

            ->with('articles', $articles);
    }

    public function getAddArticle()
    {
        $art = Content::article()->orderBy('id','DESC')->get();
        return View('admin.article.add')
             ->with('art',$art);


    }

    public function postAddArticle(ProductRequest $request)
    {
        $input = $request->all();
        $input['status'] = $request->has('status');
        $input['url']=str_replace(' ', '-',$input['url']);
//        if ($request->hasFile('image')) {
//            $path = "assets/uploads/content/art/";
//            $uploader = new UploadImg();
//            $fileName = $uploader->uploadPic($request->file('image'), $path);
//            if($fileName){
//                $input['image'] = $fileName;
//            }else{
//                return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
//            }
//        }
        if ($request->get('image_get')) {
            $fileName = mt_rand(100, 999) . ".jpg";
            file_put_contents("assets/uploads/medium/" . $fileName, file_get_contents($input['image_get']));
            file_put_contents("assets/uploads/small/" . $fileName, file_get_contents($input['image_get']));
            file_put_contents("assets/uploads/big/" . $fileName, file_get_contents($input['image_get']));
            @$input['image'] = $fileName;
        }
        $input['type'] = 'article';
        $art = Content::create($input);
        $relates = [];
        foreach($request->relates as $row){
            $relates[] = [
                'relatable_id'=>$art->id,
                'datable_id'=>$row,
                'relatable_type'=>'App\Models\Content',
                'datable_type'=>'App\Models\Content'
            ];
        }
        RelateData::insert($relates);
        return Redirect::action('Admin\ArticleController@getArticle');
    }

    public function getEditArticle($id)
    {

        $data = Content::Article()->findorfail($id);
        $art = Content::article()->get();
        $rel = RelateData::where('relatable_id',$id)->pluck('datable_id')->toArray();
        return View('admin.article.edit')
            ->with('data', $data)->with('art',$art)->with('rel',$rel);
    }

    public function postEditArticle($id, Request $request)
    {
        $input = $request->all();
        $input['status'] = $request->has('status');
        $input['url']=str_replace(' ', '-',$input['url']);
        $article = Content::Article()->find($id);
//        if ($request->hasFile('image')) {
//            $path = "assets/uploads/content/art/";
//            File::delete($path . '/big/' . $content->image);
//            File::delete($path . '/medium/' . $content->image);
//            File::delete($path . '/small/' . $content->image);
//            $uploader = new UploadImg();
//            $fileName = $uploader->uploadPic($request->file('image'), $path);
//            if($fileName){
//                $input['image'] = $fileName;
//            }else{
//                return Redirect::back()->with('error' , 'عکس ارسالی صحیح نیست.');
//            }
//        }
//        else {
//            $input['image'] = $content->image;
//        }
        if ($request->get('image_get')) {
            $path = "assets/admin/content/article/";
            File::delete($path . '/big/' . $article->image);
            File::delete($path . '/medium/' . $article->image);
            File::delete($path . '/small/' . $article->image);
            $fileName = mt_rand(100, 999) . ".jpg";
            file_put_contents("assets/uploads/medium/" . $fileName, file_get_contents($input['image_get']));
            file_put_contents("assets/uploads/small/" . $fileName, file_get_contents($input['image_get']));
            file_put_contents("assets/uploads/big/" . $fileName, file_get_contents($input['image_get']));
            @$input['image'] = $fileName;
        }
        else
        {
            $input['image'] = $article->image;
        }

        $article->update($input);
        if($request->has('relates')){
            $relates = [];
            RelateData::where('relatable_id',$article->id)->delete();
            foreach($request->relates as $row){
                $relates[] = [
                    'relatable_id'=>$article->id,
                    'datable_id'=>$row,
                    'relatable_type'=>'App\Models\Content',
                    'datable_type'=>'App\Models\Content'
                ];
            }
            RelateData::insert($relates);
        }
        return Redirect::action('Admin\ArticleController@getArticle');
    }
    public function getDeleteArticle($id)
    {

        $content = Content::find($id);
        File::delete('assets/uploads/content/art/small/' . $content->image);
        File::delete('assets/uploads/content/art/big/' . $content->image);
        File::delete('assets/uploads/content/art/medium/' . $content->image);
        Content::destroy($id);
        return Redirect::action('Admin\ArticleController@getArticle');

    }
    public function postDeleteArticle(ProductRequest $request)
    {
        $images = Content::whereIn('id', $request->get('deleteId'))->Article()->pluck('image');
        foreach ($images as $item) {
            File::delete('assets/uploads/content/art/small/' . $item);
            File::delete('assets/uploads/content/art/big/' . $item);
            File::delete('assets/uploads/content/art/medium/' . $item);
        }
        if (Content::destroy($request->get('deleteId'))) {
            return Redirect::back()
                ->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }

    }


}
