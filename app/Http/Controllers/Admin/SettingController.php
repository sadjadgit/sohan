<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
//use Classes\UploadImg;
use Classes\UploadImg;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class SettingController extends Controller
{


    public function getEditSetting()
    {
        $data = Setting::first();
        return View('admin.setting.edit')
            ->with('data', $data);
    }
    public function postEditSetting($id, Request $request)
    {
        $input = $request->all();

        $setting = Setting::find($id);
        if ($request->hasFile('logo')) {
            File::delete('assets/uploads/' . $setting->logo);
            $pathMain = "assets/uploads/";
            $extension = $request->file('logo')->getClientOriginalName();
            if (true) {
                $fileName = mt_rand(100, 999)."$extension";
                $request->file('logo')->move($pathMain, $fileName);
                $input['logo'] = $fileName;
            } else {
                return Redirect::back()->with('error', 'فایل ارسالی صحیح نیست.');
            }
        } else {
            $input['logo'] = $setting->logo;
        }
        if ($request->hasFile('logo_footer')) {
            File::delete('assets/uploads/' . $setting->logo_footer);
            $pathMain = "assets/uploads/";
            $extension = $request->file('logo_footer')->getClientOriginalName();
            if (true) {
                $fileName = mt_rand(100, 999)."$extension";
                $request->file('logo_footer')->move($pathMain, $fileName);
                $input['logo_footer'] = $fileName;
            } else {
                return Redirect::back()->with('error', 'فایل ارسالی صحیح نیست.');
            }
        } else {
            $input['logo_footer'] = $setting->logo_footer;
        }
        if ($request->hasFile('logo_water_mark')) {
            File::delete('assets/uploads/content/set/' . $setting->logo_water_mark);
            $pathMain = "assets/uploads/content/set";
            $extension = $request->file('logo_water_mark')->getClientOriginalName();
            if (true) {
                $fileName = mt_rand(100, 999)."$extension";
                $request->file('logo_water_mark')->move($pathMain, $fileName);
                $input['logo_water_mark'] = $fileName;
            } else {
                return Redirect::back()->with('eror', 'فایل ارسالی صحیح نیست.');
            }
        } else {
            $input['logo_water_mark'] = $setting->logo_water_mark;
        }
        if ($request->hasFile('aboutimg')) {
            File::delete('assets/uploads/content/set/' . $setting->aboutimg);
            $pathMain = "assets/uploads/content/set";
            $extension = $request->file('aboutimg')->getClientOriginalName();
            if (true) {
                $fileName = mt_rand(100, 999)."$extension";
                $request->file('aboutimg')->move($pathMain, $fileName);
                $input['aboutimg'] = $fileName;
            } else {
                return Redirect::back()->with('eror', 'فایل ارسالی صحیح نیست.');
            }
        } else {
            $input['aboutimg'] = $setting->aboutimg;
        }

        if ($request->hasFile('favicon')) {
            File::delete('assets/uploads/content/set/' . $setting->favicon);
            $pathMain = "assets/uploads/content/set";
            $extension = $request->file('favicon')->getClientOriginalName();
            if (true) {
                $fileName = mt_rand(100, 999)."$extension";
                $request->file('favicon')->move($pathMain, $fileName);
                $input['favicon'] = $fileName;
            } else {
                return Redirect::back()->with('eror', 'فایل ارسالی صحیح نیست.');
            }
        } else {
            $input['favicon'] = $setting->favicon;
        }
        $setting = Setting::first();
        $setting->update($input);
        return Redirect::action('Admin\SettingController@getEditSetting')->with('success', 'آیتم مورد نظر با موفقیت ویرایش شد.');    }

}
