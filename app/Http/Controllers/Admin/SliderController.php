<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\BannerRequest;
use App\Http\Requests\StoreUserRequest;
use App\Models\Content;
use Classes\UploadImg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class SliderController extends Controller
{
//    /**
//     * Create a new controller instance.
//     *
//     * @return void
//     */
//    public function __construct()
//    {
//        $this->middleware('auth:admin');
//    }
//
//    /**
//     * Show the application dashboard.
//     *
//     * @return \Illuminate\Contracts\Support\Renderable
//     */


    public function getSlider()
    {
        $slider = Content::orderBy('id','DESC')->Slider()->paginate(100);

        return View('admin.slider.index')

            ->with('slider', $slider);
    }

    public function getAddSlider()
    {
        return View('admin.slider.add');

    }

    public function postAddSlider(Request $request)
    {
        $input = $request->all();
        $input['status'] = $request->has('status');
//        if ($request->hasFile('image')) {
//            $pathMain = "assets/uploads/content/sli";
//            $extensionf = $request->file('image')->getClientOriginalName();
//            $fileName =mt_rand(100, 999)."$extensionf";
//            $request->file('image')->move($pathMain, $fileName);
//            $input['image'] = $fileName;
//        }
        if ($request->get('image_get')) {
            $fileName = mt_rand(100, 999) . ".jpg";
            file_put_contents("assets/uploads/medium/" . $fileName, file_get_contents($input['image_get']));
            file_put_contents("assets/uploads/small/" . $fileName, file_get_contents($input['image_get']));
            file_put_contents("assets/uploads/big/" . $fileName, file_get_contents($input['image_get']));
            @$input['image'] = $fileName;
        }
        $input['type'] = 'slider';
        $slider = Content::create($input);
        return Redirect::action('Admin\SliderController@getSlider');
    }

    public function getEditSlider($id)
    {
        $data = Content::orderBy('id','DESC')->Slider()->findorfail($id);
        return View('admin.slider.edit')
            ->with('data', $data);
    }

    public function postEditSlider($id, Request $request)
    {
        $input = $request->all();
        $input['status'] = $request->has('status');
        $content = Content::find($id);
//        if ($request->hasFile('image')) {
//            File::delete('assets/uploads/content/sli/' . $content->image);
//            $pathMain = "assets/uploads/content/sli";
//            $extension = $request->file('image')->getClientOriginalName();
//            if (true) {
//                $fileName =mt_rand(100, 999)."$extension";
//                $request->file('image')->move($pathMain, $fileName);
//                $input['image'] = $fileName;
//            } else {
//                return Redirect::back()->with('eror', 'فایل ارسالی صحیح نیست.');
//            }
//        } else {
//            $input['image'] = $content->image;
//        }
        if ($request->get('image_get')) {
            $path = "assets/admin/content/article/";
            File::delete($path . '/big/' . $content->image);
            File::delete($path . '/medium/' . $content->image);
            File::delete($path . '/small/' . $content->image);
            $fileName = mt_rand(100, 999) . ".jpg";
            file_put_contents("assets/uploads/medium/" . $fileName, file_get_contents($input['image_get']));
            file_put_contents("assets/uploads/small/" . $fileName, file_get_contents($input['image_get']));
            file_put_contents("assets/uploads/big/" . $fileName, file_get_contents($input['image_get']));
            @$input['image'] = $fileName;
        }
        else
        {
            $input['image'] = $content->image;
        }
        $slider = Content::orderBy('id','DESC')->Slider()->find($id);
        $slider->update($input);
        return Redirect::action('Admin\SliderController@getSlider');
    }
    public function getDeleteSlider($id)
    {

        $content = Content::find($id);
        File::delete('assets/uploads/medium/' . $content->image);
        File::delete('assets/uploads/small/' . $content->image);
        File::delete('assets/uploads/big/' . $content->image);
        Content::destroy($id);
        return Redirect::action('Admin\SliderController@getSlider');

    }

    public function postDeleteSlider(Request $request)
    {
        if (Content::destroy($request->get('deleteId'))) {
            return Redirect::back()
                ->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }

    }


}
