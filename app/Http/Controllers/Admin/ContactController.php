<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ContactExport;
use App\Exports\MessageExport;
use App\Models\Comments;

use App\Models\Contact;
use App\Models\Content;
use App\Models\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Classes\UploadImg;
use Illuminate\Support\Facades\Redirect;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Facades\Excel as MaatExcel;


class ContactController extends Controller
{
    public function getIndex(Request $request)
    {
//        $query = Contact::query();
//        if ($request->has('search')) {
//            if ($request->has('title')) {
//                $query->where('title', 'LIKE', '%' . $request->get('title') . '%');
//            }
//            if ($request->has('id')) {
//                $query->where('id', $request->get('id'));
//            }
//
//        }
//        $data = $query->latest()->paginate(15);
//
//        $all = Contact::whereStatus(1)->orderby('listorder', 'ASC')->get();

        $data = Contact::paginate(15);
        return View('admin.contacts.index')
            ->with('data', $data);
//            ->with('all', $all)
//            ->with('data', $data);

    }
    public function getEdit($id)
    {

        $data = Contact::findorfail($id);



        return View('admin.contacts.edit')
            ->with('data', $data);


    }
    public function postEdit($id, Request $request)
    {

        $input = $request->all();
        $contacts = Contact::find($id);
        $input['readat'] = '1';


        $contacts->update($input);
        return Redirect::action('Admin\ContactController@getIndex')
            ->with('success', 'آیتم مورد نظر با موفقیت ویرابش شد.');
    }
    public function postDelete($id)
    {
        $content = Contact::find($id);
        Contact::destroy($id);
        return Redirect::action('Admin\ContactController@getIndex')
            ->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');

    }
//    public function postSort(Request $request)
//    {
//        if ($request->get('update') == "update") {
//            $count = 1;
//            if ($request->get('update') == 'update') {
//                foreach ($request->get('arrayorder') as $idval) {
//                    $category = Contact::find($idval);
//                    $category->listorder = $count;
//                    $category->save();
//                    $count++;
//                }
//                echo 'با موفقیت ذخیره شد.';
//            }
//
//        }
//
//    }
    public function getExcel()
    {


        return Excel::download(new ContactExport, 'contacts.xlsx');
    }
}
