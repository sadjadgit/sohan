<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\StoreUserRequest;
use App\Models\Content;

use Classes\UploadImg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class SampleController extends Controller
{
//    /**
//     * Create a new controller instance.
//     *
//     * @return void
//     */
//    public function __construct()
//    {
//        $this->middleware('auth:admin');
//    }
//
//    /**
//     * Show the application dashboard.
//     *
//     * @return \Illuminate\Contracts\Support\Renderable
//     */
    public function getSample()
    {
        $samples = Content::orderBy('id','DESC')->Sample()->paginate(100);


        return View('admin.samples.index')

            ->with('samples', $samples);
    }

    public function getAddSample()
    {


        return View('admin.samples.add');


    }

    public function postAddSample(Request $request)
    {
        $input = $request->all();

        if ($request->hasFile('image')) {
            $pathMain = "assets/uploads/content/sample";
            $extensionf = $request->file('image')->getClientOriginalName();
            $fileName = mt_rand(100, 999)."$extensionf";
            $request->file('image')->move($pathMain, $fileName);
            $input['image'] = $fileName;
        }
        $input['type'] = 'sample';
        $Sample = Content::create($input);
        return Redirect::action('Admin\SampleController@getSample');
    }

    public function getEditSample($id)
    {
        $data = Content::orderBy('id','DESC')->Sample()->findorfail($id);
        return View('admin.samples.edit')
            ->with('data', $data);
    }

    public function postEditSample($id, Request $request)
    {
        $input = $request->all();
        $content = Content::find($id);

        if ($request->hasFile('image')) {
            File::delete('assets/uploads/content/sample/' . $content->image);
            $pathMain = "assets/uploads/content/sample";
            $extensionf = $request->file('image')->getClientOriginalName();
            if (true) {
                $fileName = mt_rand(100, 999)."$extensionf";
                $request->file('image')->move($pathMain, $fileName);
                $input['image'] = $fileName;
            } else {
                return Redirect::back()->with('eror', 'فایل ارسالی صحیح نیست.');
            }
        } else {
            $input['image'] = $content->image;
        }

        $Sample = Content::orderBy('id','DESC')->Sample()->find($id);
        $input['type'] = 'sample';
        $Sample->update($input);
        return Redirect::action('Admin\SampleController@getSample');
    }
    public function getDeleteSample($id)
    {

        $content = Content::find($id);
        File::delete('assets/uploads/content/sample/small/' . $content->image);
        File::delete('assets/uploads/content/sample/big/' . $content->image);
        File::delete('assets/uploads/content/sample/medium/' . $content->image);
        Content::destroy($id);
        return Redirect::action('Admin\SampleController@getSample');

    }
    public function postDeleteSample(Request $request)
    {
        $images = Content::whereIn('id', $request->get('deleteId'))->Sample()->pluck('image');
        foreach ($images as $item) {
            File::delete('assets/uploads/content/sample/small/' . $item);
            File::delete('assets/uploads/content/sample/big/' . $item);
            File::delete('assets/uploads/content/sample/medium/' . $item);
        }
        if (Content::destroy($request->get('deleteId'))) {
            return Redirect::back()
                ->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }

    }



}
