<?php

namespace App\Http\Controllers\admin;

use App\Model\Redirects;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class RedirectController extends Controller
{
    //---Redirects-----
    public function getRedirect(){

        $redirect = Redirects::paginate(15);

        return view('admin.redirect.list')
            ->with('redirect', $redirect);
    }
    public function getRedirectAdd(){
        return view('admin.redirect.add');

    }
    public function postRedirectAdd(Request $request){
        $input=$request->all();
        $remove = array(url('/'.@$input->old_address).'/');
        $remove2 = array(url('/'.@$input->new_address).'/');
        $input['old_address']=str_replace($remove, "", $input['old_address']);
        $input['new_address']=str_replace($remove2, "", $input['new_address']);
        Redirects::create($input);


        return Redirect::action('Admin\RedirectController@getRedirect');

    }
    public function getRedirectDelete($id){
        Redirects::destroy($id);
        return Redirect::action('Admin\RedirectController@getRedirect');
    }

}
