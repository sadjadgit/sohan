<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Social;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class SocialController extends Controller
{
    public function getSocial()
    {
        $social = Social::first();
        return View('admin.socials.index')
            ->with('social', $social);
    }

    public function getAddSocial()
    {
        return View('admin.socials.add');


    }

    public function postAddSocial(Request $request)
    {
        $input = $request->all();
        $social = Social::create($input);
        return Redirect::action('Admin\SocialController@getSocial');
    }

    public function getEditSocial($id)
    {

        $data = Social::orderBy('id','DESC')->findorfail($id);
        return View('admin.socials.edit')
            ->with('data', $data);
    }

    public function postEditSocial($id, Request $request)
    {
        $input = $request->all();
        $content = Social::find($id);

        $social = Social::find($id);
        $social->update($input);
        return Redirect::action('Admin\SocialController@getSocial');
    }
    public function getDeleteSocial($id)
    {

        $social = Social::find($id);
        Social::destroy($id);
        return Redirect::action('Admin\SocialController@getSocial');

    }
}
