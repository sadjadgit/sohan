<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\InsRequest;
use App\Http\Requests\StoreUserRequest;
use App\Models\Content;
use App\Models\Insurance;
use Classes\UploadImg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class WhyController extends Controller
{
//    /**
//     * Create a new controller instance.
//     *
//     * @return void
//     */
//    public function __construct()
//    {
//        $this->middleware('auth:admin');
//    }
//
//    /**
//     * Show the application dashboard.
//     *
//     * @return \Illuminate\Contracts\Support\Renderable
//     */
    public function get()
    {
        $insures = Content::orderBy('id','DESC')->Why()->paginate(100);


        return View('admin.why.index')
            ->with('insures', $insures);
    }
    public function getAdd()
    {

        return View('admin.why.add');


    }

    public function postAdd(Request $request)
    {
        $input = $request->all();
        $arr = [];
        foreach ($input['title'] as $key=>$item){
            $arr[] = [

                'title'=>$item,
                'type'=>'why',
            ];
        }

        Content::insert($arr);
        return Redirect::action('Admin\WhyController@get');
    }

    public function getEdit($id)
    {
        $data = Content::orderBy('id','DESC')->findorfail($id);


        return View('admin.why.edit')
            ->with('data', $data);
    }

    public function postEdit($id, Request $request)
    {
        $input = $request->all();
        $input['status'] = $request->has('status');
        $content = Content::find($id);

        $banner = Content::orderBy('id','DESC')->Ins()->find($id);
        $banner->update($input);
        return Redirect::action('Admin\WhyController@get');
    }
    public function getDelete($id)
    {

        $content = Content::find($id);
//        File::delete('assets/uploads/content/banner/' . $content->image);
        Content::destroy($id);
        return Redirect::action('Admin\WhyController@get');

    }
    public function postDelete(Request $request)
    {
        if (Content::destroy($request->get('deleteId'))) {
            return Redirect::back()
                ->with('success', 'کدهای مورد نظر با موفقیت حذف شدند.');
        }

    }

}
