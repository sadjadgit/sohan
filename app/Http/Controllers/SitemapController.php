<?php

namespace App\Http\Controllers;

use App\Model\Content;
use Illuminate\Http\Request;

class SitemapController extends Controller
{
    public function sitemap()
    {
        $products = Content::orderby('id', 'DESC')->Cars()->get();
        $articles = Content::Article()->get();
        $slo = Content::Sloagens()->get();
        $customer = Content::Cus()->get();
        return response()->view('sitemap', [

            'products' => $products,
            'articles' => $articles,
            'slo' => $slo,
            'customer' => $customer,

        ])->header('Content-Type','text/xml');    }
}
