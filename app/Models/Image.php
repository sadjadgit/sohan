<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{
    use SoftDeletes;
   protected $fillable = [
     'image' , 'content_id'
   ];

    public function content()
    {
        return $this->belongsTo('App\Models\Content','content_id');
    }


}
