<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Maatwebsite\Excel\Concerns\FromCollection;

class Contact extends Model
{
    protected $table = 'contact';
    use SoftDeletes;
    protected $fillable = [
        'name', 'email', 'message','subject','phone', 'readat'
    ];
}
