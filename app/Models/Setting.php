<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Setting extends Model
{
    protected $table = 'setting';
    use SoftDeletes;
    protected $fillable = [
        'title', 'logo','abouttitle', 'about', 'about2', 'footer_about',
        'aboutimg', 'contact', 'phone', 'maps', 'email', 'address', 'description_seo','favicon',
        'rules','image1','image2','factory_address','logo_footer','postal_code'
    ];


}
