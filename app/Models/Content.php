<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends Model
{
    protected $table = 'contents';
    use SoftDeletes;
    protected $fillable = [
        'title', 'description', 'image','status', 'type', 'parent_id', 'description_seo',
        'url', 'title_seo', 'img_seo', 'lead','title_en','color',

    ];
    public function scopeCat($query)
    {
        $records = $query->whereType('cat');
        return $records;
    }
    public function scopeProduct($query)
    {
        $records = $query->whereType('product');
        return $records;
    }
    public function scopeCatArticle($query)
    {
        $records = $query->whereType('catArticle');
        return $records;
    }
    public function scopeArticle($query)
    {
        $records = $query->whereType('article');
        return $records;
    }
    public function scopeSlider($query)
    {
        $records = $query->whereType('slider');
        return $records;
    }
    public function scopeSample($query)
    {
        $records = $query->whereType('sample');
        return $records;
    }
    public function scopeUpl($query)
    {
        $records = $query->whereType('upl');
        return $records;
    }
    public function scopeBnr($query)
    {
        $records = $query->whereType('bnr');
        return $records;
    }

    public function scopeSlogan($query)
    {
        $records = $query->whereType('slogan');
        return $records;
    }



    public function scopeWhy($query)
    {
        $records = $query->whereType('why');
        return $records;
    }
    public function category()
    {
        return $this->belongsTo(\App\Models\Content::class, 'parent_id', 'id');
    }

    public function childs()
    {
        return $this->hasMany('App\Models\Content','parent_id')->with('childs');
    }


    public function messages()
    {
        return $this->morphMany('App\Models\Message', 'Messageable')->whereNull('parent_id');
    }
    public function images()
    {
        return $this->hasMany('App\Models\Image');
    }



}
