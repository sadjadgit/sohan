<?php

use App\Models\Content;
use App\Models\Redirects;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    
Route::get('/test', function () {
    $exitCode1 = Artisan::call('cache:clear');
    $exitCode2 = Artisan::call('config:clear');
    $exitCode3 = Artisan::call('route:clear');
    return '<h1>Cache facade value cleared</h1>';
});

Route::get('/test2', function () {
    $exitCode1 = Artisan::call('key:generate');
    $exitCode2 = Artisan::call('config:cache');
    return '<h1>  value cleared</h1>';
});

//===============================site=============================
Route::get('/', 'Site\HomeController@getIndex');
Route::get('/search', 'Site\HomeController@getSearch');
Route::get('/about-us', 'Site\HomeController@getAbout');
Route::get('/contact-us', 'Site\HomeController@getContact');
Route::post('/contact-us', 'Site\HomeController@postContact');
Route::get('/news', 'Site\ContentController@getArticleList');
//Route::get('/products', 'Site\ProductController@getProductList');

$category = Content::cat()->get();
foreach ($category as $row) {
    if (@$row->url) {
        if (@$row->url) {
            Route::get('/'.@$row->url, 'Site\ProductController@getProductList');
        }
    }
}


$pro = Content::product()->get();
foreach ($pro as $row) {
    if (@$row->url) {
        if (@$row->url) {
            Route::get('/'.@$row->url, 'Site\ProductController@getProductDetails');
        }
    }
}

$art = Content::article()->get();
foreach ($art as $row) {
    if (@$row->url) {
        if (@$row->url) {
            Route::get('/'.@$row->url, 'Site\ContentController@getArticleDetails');
        }
    }
}





Route::get('/article-details', function () {
    return view('site.article.details');
});








///------------------------------------Login--------------------------
Route::get('admin/login', 'Admin\LoginController@getLogin');
Route::get('/login', 'Admin\LoginController@getLogin');
Route::post('admin/login', 'Admin\LoginController@postLogin');
Route::post('/login', 'Admin\LoginController@postLogin');
Route::get('admin/logout', function () {
    \Illuminate\Support\Facades\Auth::logout();
    return redirect()->to('/login');
});

//-------------------Admin----------------------------------------------------
Route::namespace('Admin')->prefix('admin')->group(function () {
    Route::middleware('AdminPermission')->group(function () {
        Route::get('/', 'ContentController@getAdmin');

        //--------------------------------cropper-----------------------------------
        Route::get('/cropper', 'ContentController@getCropper');
        Route::get('/cropper', 'ContentController@getCropper');
        //----------------------Product--------------------------------------
        Route::get('products', 'ProductController@getProduct');
        Route::get('products/add', 'ProductController@getAddProduct');
        Route::post('products/add', 'ProductController@postAddProduct');
        Route::get('products/delete/{id}', 'ProductController@getDeleteProduct');
        Route::post('products/delete', 'ProductController@postDeleteProduct');
        Route::get('products/edit/{id}', 'ProductController@getEditProduct');
        Route::post('products/edit/{id}', 'ProductController@postEditProduct');
        Route::post('products/sort', 'ProductController@postSort');
        //----------------------Product image--------------------------------------
        Route::get('/products/image/{id}', 'ProductController@getImage');
        Route::get('/products/image/add/{id}', 'ProductController@getAddImage');
        Route::post('/products/image/add', 'ProductController@postAddImage');
        Route::get('/products/image/edit/{id}', 'ProductController@getEditImage');
        Route::post('/products/image/edit/{id}', 'ProductController@postEditImage');
        Route::post('/products/image/delete', 'ProductController@postDeleteImage');
        //----------------------Sample--------------------------------------
        Route::get('samples', 'SampleController@getSample');
        Route::get('samples/add', 'SampleController@getAddSample');
        Route::post('samples/add', 'SampleController@postAddSample');
        Route::get('samples/delete/{id}', 'SampleController@getDeleteSample');
        Route::post('samples/delete', 'SampleController@postDeleteSample');
        Route::get('samples/edit/{id}', 'SampleController@getEditSample');
        Route::post('samples/edit/{id}', 'SampleController@postEditSample');
        Route::post('samples/sort', 'SampleController@postSort');
        //----------------------Sample--------------------------------------
        Route::get('slogan', 'ContentController@getSlogan');
        Route::get('slogan/add', 'ContentController@getAddSlogan');
        Route::post('slogan/add', 'ContentController@postAddSlogan');
        Route::get('slogan/delete/{id}', 'ContentController@getDeleteSlogan');
        Route::post('slogan/delete', 'ContentController@postDeleteSlogan');
        Route::get('slogan/edit/{id}', 'ContentController@getEditSlogan');
        Route::post('slogan/edit/{id}', 'ContentController@postEditSlogan');
        Route::post('slogan/sort', 'ContentController@postSort');
        //----------------------why--------------------------------------
        Route::get('why', 'WhyController@get');
        Route::get('why/add', 'WhyController@getAdd');
        Route::post('why/add', 'WhyController@postAdd');
        Route::get('why/delete/{id}', 'WhyController@getDelete');
        Route::post('why/delete', 'WhyController@postDelete');
        Route::get('why/edit/{id}', 'WhyController@getEdit');
        Route::post('why/edit/{id}', 'WhyController@postEdit');


        //----------------------Cat--------------------------------------
        Route::get('category', 'CategoryController@getCategory');
        Route::get('category/add', 'CategoryController@getAddCategory');
        Route::post('category/add', 'CategoryController@postAddCategory');
        Route::get('category/delete/{id}', 'CategoryController@getDeleteCategory');
        Route::post('category/delete', 'CategoryController@postDeleteCategory');
        Route::get('category/edit/{id}', 'CategoryController@getEditCategory');
        Route::post('category/edit/{id}', 'CategoryController@postEditCategory');

        //----------------------Sli--------------------------------------
        Route::get('slider', 'SliderController@getSlider');
        Route::get('slider/add', 'SliderController@getAddSlider');
        Route::post('slider/add', 'SliderController@postAddSlider');
        Route::get('slider/delete/{id}', 'SliderController@getDeleteSlider');
        Route::post('slider/delete', 'SliderController@postDeleteSlider');
        Route::get('slider/edit/{id}', 'SliderController@getEditSlider');
        Route::post('slider/edit/{id}', 'SliderController@postEditSlider');

        ////-----------------------------------sloagens------------------------------------
        Route::get('/sloagens', 'SloagenController@getSloagens');
        Route::get('sloagens/add', 'SloagenController@getAddSloagens');
        Route::post('sloagens/add', 'SloagenController@postAddSloagens');
        Route::get('sloagens/delete/{id}', 'SloagenController@getDeleteSloagens');
        Route::post('sloagens/delete', 'SloagenController@postDeleteSloagens');
        Route::get('sloagens/edit/{id}', 'SloagenController@getEditSloagens');
        Route::post('sloagens/edit/{id}', 'SloagenController@postEditSloagens');
        //---------------------Setting--------------------------------------------

        Route::get('setting/', 'SettingController@getEditSetting');
        Route::post('setting/edit/{id}', 'SettingController@postEditSetting');
        //---------------------Sitemap--------------------------------------------
        Route::get('sitemap', 'SettingController@getEditSitemap');
        Route::post('sitemap/edit/{id}', 'SettingController@postEditSitemap');
        //-----------------------------Article-------------------------------------------
        Route::get('articles', 'ArticleController@getArticle');
        Route::get('articles/add', 'ArticleController@getAddArticle');
        Route::post('articles/add', 'ArticleController@postAddArticle');
        Route::get('articles/delete/{id}', 'ArticleController@getDeleteArticle');
        Route::post('articles/delete', 'ArticleController@postDeleteArticle');
        Route::get('articles/edit/{id}', 'ArticleController@getEditArticle');
        Route::post('articles/edit/{id}', 'ArticleController@postEditArticle');
        Route::post('articles/sort', 'ArticleController@postSort');
        //---------------------------Upl--------------------------------------------------
        Route::get('uploader', 'ContentController@getUploader');
        Route::get('uploader/add', 'ContentController@getAddUploader');
        Route::post('uploader/add', 'ContentController@postAddUploader');
        Route::get('uploader/delete/{id}', 'ContentController@getDeleteUploader');
        Route::get('uploader/edit/{id}', 'ContentController@getEditUploader');
        Route::post('uploader/edit/{id}', 'ContentController@postEditUploader');
        //-------------------------------redirect-----------------------------------------
        Route::get('redirect', 'RedirectController@getRedirect');
        Route::get('redirect/add', 'RedirectController@getRedirectAdd');
        Route::post('redirect/add', 'RedirectController@postRedirectAdd');
        Route::get('redirect/delete/{id}', 'RedirectController@getRedirectDelete');
        //---------------------------------social---------------------------------------------
        Route::get('socials', 'SocialController@getSocial');
        Route::get('socials/add', 'SocialController@getAddSocial');
        Route::post('socials/add', 'SocialController@postAddSocial');
        Route::get('socials/delete/{id}', 'SocialController@getDeleteSocial');
        Route::get('socials/edit/{id}', 'SocialController@getEditSocial');
        Route::post('socials/edit/{id}', 'SocialController@postEditSocial');
        //---------------------comment-----------------------------
        Route::get('comment', 'CommentController@getIndex');
        Route::get('comment/edit/{id}', 'CommentController@getEdit');
        Route::post('comment/edit/{id}', 'CommentController@postEdit');
        Route::post('comment/delete', 'CommentController@postDelete');
        Route::get('comment/excel', 'CommentController@getExcel');
        //---------------------contact-----------------------------
        Route::get('contact', 'ContactController@getIndex');
        Route::get('contact/edit/{id}', 'ContactController@getEdit');
        Route::post('contact/edit/{id}', 'ContactController@postEdit');
        Route::post('contact/delete', 'ContactController@postDelete');
        Route::get('contact/excel', 'ContactController@getExcel');
        //-------------------------------------------------------------------------------------------------
    });
});


$route=Redirects::get();
foreach ($route as $row) {
//    log::info($row->old_address.'----');
//
//    log::info($row->new_address);
    Route::get('/'.urldecode($row->old_address), function () use ($row) {
        return redirect('/'.$row->new_address);
    });
}
